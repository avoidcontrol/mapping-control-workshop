const incidents = {
  "type": "FeatureCollection",
  "features": [
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Zoologischer Garten",
          "id": "900000023201"
        },
        "direction": null,
        "line": {
          "name": "U9",
          "id": null
        },
        "timestamp": "1581850375000",
        "text": "RT @_morre_: 11:52 Kontrolle im U Zoologischer Garten (U9) #noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.332707,
          52.506921
        ],
        "type": "Point"
      },
      "id": "0408e04bdebd03cb544a53bd330e25f6"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Warschauer Str",
          "id": "900000120004"
        },
        "direction": null,
        "line": {
          "name": "U1",
          "id": null
        },
        "timestamp": "1583773837000",
        "text": "RT @niklasAuBLn: U1/U3 drei Kerle gerade Wahrschauer Straße ausgestiegen @noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.448721,
          52.505889
        ],
        "type": "Point"
      },
      "id": "05aefe6eb0ad8975c6d6db714efc2379"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Nöldnerplatz",
          "id": "900000160003"
        },
        "direction": null,
        "line": {
          "name": "S7",
          "id": null
        },
        "timestamp": "1583137793000",
        "text": "RT @frl_rastlos: #noticket_bln S7 Richtung Potsdam Höhe Nöldnerplatz Kontrolle. \n\nMind. 2 weibliche Kontrolleurinnen."
      },
      "geometry": {
        "coordinates": [
          13.484297,
          52.503444
        ],
        "type": "Point"
      },
      "id": "08527a43ba66593444455e537d187801"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Potsdamer Platz",
          "id": "900000100020"
        },
        "direction": null,
        "line": {
          "name": "S1",
          "id": null
        },
        "timestamp": "1573150916000",
        "text": "RT @sim030: @noticket_bln hmm. Touri Kontros Nummer 70354\n\n#S1 Potsdamer Platz Richtung Norden\n\n#noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.376454,
          52.50934
        ],
        "type": "Point"
      },
      "id": "089b2fcc7522b9ccf0279256efafcc0a"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": {
          "name": "S3",
          "id": null
        },
        "timestamp": "1583414127000",
        "text": "RT @hackthesociety: @noticket_bln three guys S3 HBF -&gt; Erkner"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "09254d90225f5f24a09466d66098f02e"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Schlesisches Tor",
          "id": "900000014102"
        },
        "direction": null,
        "line": null,
        "timestamp": "1582703985000",
        "text": "RT @SiCaminou: #noticket_bln U 1 Schlesi richtung Uhland, einer hat blaue Jacke mit Fellkragen"
      },
      "geometry": {
        "coordinates": [
          13.441791,
          52.501147
        ],
        "type": "Point"
      },
      "id": "0c72371c6aa5e04e3976e43adb17f55f"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Schlachtensee",
          "id": "900000050355"
        },
        "direction": {
          "name": "Wannsee",
          "id": "900000053301"
        },
        "line": {
          "name": "S1",
          "id": null
        },
        "timestamp": "1575269233000",
        "text": "RT @ArthurPobel: #noticket_bln kontrolle S1 Richtung Wannsee kurz vor Schlachtensee"
      },
      "geometry": {
        "coordinates": [
          13.213986,
          52.439806
        ],
        "type": "Point"
      },
      "id": "0e2c8536fa86b9a45cb8cd721c237c46"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Alexanderplatz",
          "id": "900000100003"
        },
        "direction": null,
        "line": null,
        "timestamp": "1581498991000",
        "text": "RT @naziwatch_brb: Zwei BVG Kontrolleure in Uniform an der S-Bahn Alexanderplatz #noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.411267,
          52.521512
        ],
        "type": "Point"
      },
      "id": "0e72c2df66144cd01993732ee682611c"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Turmstr",
          "id": "900000003104"
        },
        "direction": null,
        "line": {
          "name": "U9",
          "id": null
        },
        "timestamp": "1508758452000",
        "text": "RT @NickPower3939: @ber_ohne_ticket Großkontrolle Turmstraße! #u9"
      },
      "geometry": {
        "coordinates": [
          13.341417,
          52.525938
        ],
        "type": "Point"
      },
      "id": "1177d4635f2bd78d9a03bcf959e54d6e"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Kleistpark",
          "id": "900000054102"
        },
        "direction": null,
        "line": {
          "name": "U2",
          "id": null
        },
        "timestamp": "1580485953000",
        "text": "RT @atari_terror: @noticket_bln Kontrolle U2 Kleistpark...vor so 20 Minuten 🙄"
      },
      "geometry": {
        "coordinates": [
          13.360917,
          52.490845
        ],
        "type": "Point"
      },
      "id": "130295d1f2c2d70e3d037833eba9d710"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Wildau",
          "id": "900000260002"
        },
        "direction": null,
        "line": {
          "name": "S46",
          "id": null
        },
        "timestamp": "1576595509000",
        "text": "RT @lysalia_: @noticket_bln #noticket_bln\n16:10\nS46 Königs Wusterhausen\nAb wildau\nP2 hat nen grünen Mantel an"
      },
      "geometry": {
        "coordinates": [
          13.633587,
          52.319205
        ],
        "type": "Point"
      },
      "id": "13b14586107c5347046ff65fc64bd45f"
    },
    {
      "type": "Feature",
      "properties": {
        "someInfo": "hello"
      },
      "geometry": {
        "coordinates": [
          10,
          10
        ],
        "type": "Point"
      },
      "id": "1586775503396"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Warschauer Str",
          "id": "900000120004"
        },
        "direction": null,
        "line": null,
        "timestamp": "1586809825750",
        "text": "I'm gay here at the Warschauer strasse"
      },
      "geometry": {
        "coordinates": [
          13.448721,
          52.505889
        ],
        "type": "Point"
      },
      "id": "1586809831035"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Leinestr",
          "id": "900000079201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590134655619",
        "text": "this text contains a station, and that station is leinestrasse"
      },
      "geometry": {
        "coordinates": [
          13.4284,
          52.472874
        ],
        "type": "Point"
      },
      "id": "1590134660813"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Potsdamer Platz",
          "id": "900000100020"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590148476972",
        "text": "Most controlled stations:\n\nAlexanderplatz: 9\nTempelhof: 9\nRosenthaler Platz: 8\nFriedrichstr: 7\nBerlin Hauptbahnhof: 6\nBellevue: 6\nWarschauer Str: 5\nGörlitzer Bahnhof: 5\nMöckernbrücke: 5\nPotsdamer Platz: 3\n\nFrom all collected data"
      },
      "geometry": {
        "coordinates": [
          13.376454,
          52.50934
        ],
        "type": "Point"
      },
      "id": "1590148482306"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Potsdamer Platz",
          "id": "900000100020"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590393802664",
        "text": "Most controlled stations:\n\nAlexanderplatz: 9\nTempelhof: 9\nRosenthaler Platz: 8\nFriedrichstr: 7\nBerlin Hauptbahnhof: 6\nBellevue: 6\nWarschauer Str: 5\nGörlitzer Bahnhof: 5\nMöckernbrücke: 5\nPotsdamer Platz: 4\n\nFrom all collected data"
      },
      "geometry": {
        "coordinates": [
          13.376454,
          52.50934
        ],
        "type": "Point"
      },
      "id": "1590393807970"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590400800587",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Möckernbrücke\n🚉 Tempelhof\n\nDistricts:\n🗺️ Mitte\n🗺️ Kreuzberg\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Friedrichshain\n\nSince 12:00 25.4.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1590400805827"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590415200637",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Möckernbrücke\n🚉 Tempelhof\n\nDistricts:\n🗺️ Mitte\n🗺️ Kreuzberg\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Friedrichshain\n\nSince 16:00 25.4.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1590415207037"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590429600702",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Möckernbrücke\n🚉 Tempelhof\n\nDistricts:\n🗺️ Mitte\n🗺️ Kreuzberg\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Friedrichshain\n\nSince 20:00 25.4.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1590429605926"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590444000699",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Möckernbrücke\n🚉 Tempelhof\n\nDistricts:\n🗺️ Mitte\n🗺️ Kreuzberg\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Friedrichshain\n\nSince 0:00 26.4.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1590444006068"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590487200943",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Möckernbrücke\n🚉 Tempelhof\n\nDistricts:\n🗺️ Mitte\n🗺️ Kreuzberg\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Friedrichshain\n\nSince 12:00 26.4.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1590487206186"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590501601018",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Möckernbrücke\n🚉 Tempelhof\n\nDistricts:\n🗺️ Mitte\n🗺️ Kreuzberg\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Friedrichshain\n\nSince 16:00 26.4.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1590501606986"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590516000982",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Möckernbrücke\n🚉 Tempelhof\n\nDistricts:\n🗺️ Mitte\n🗺️ Kreuzberg\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Friedrichshain\n\nSince 20:00 26.4.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1590516006281"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590530401126",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Möckernbrücke\n🚉 Tempelhof\n\nDistricts:\n🗺️ Mitte\n🗺️ Kreuzberg\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Friedrichshain\n\nSince 0:00 27.4.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1590530406648"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590544801198",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Möckernbrücke\n🚉 Tempelhof\n\nDistricts:\n🗺️ Mitte\n🗺️ Kreuzberg\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Friedrichshain\n\nSince 4:00 27.4.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1590544806487"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590559201175",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Möckernbrücke\n🚉 Tempelhof\n\nDistricts:\n🗺️ Mitte\n🗺️ Kreuzberg\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Friedrichshain\n\nSince 8:00 27.4.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1590559207220"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590573601181",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Möckernbrücke\n🚉 Tempelhof\n\nDistricts:\n🗺️ Mitte\n🗺️ Kreuzberg\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Friedrichshain\n\nSince 12:00 27.4.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1590573606434"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590588001268",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Möckernbrücke\n🚉 Tempelhof\n\nDistricts:\n🗺️ Mitte\n🗺️ Kreuzberg\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Friedrichshain\n\nSince 16:00 27.4.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1590588007023"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590602401307",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Möckernbrücke\n🚉 Tempelhof\n\nDistricts:\n🗺️ Mitte\n🗺️ Kreuzberg\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Friedrichshain\n\nSince 20:00 27.4.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1590602406903"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590660001715",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Möckernbrücke\n🚉 Tempelhof\n\nDistricts:\n🗺️ Mitte\n🗺️ Kreuzberg\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Friedrichshain\n\nSince 12:00 28.4.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1590660007326"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590674401655",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Möckernbrücke\n🚉 Tempelhof\n\nDistricts:\n🗺️ Mitte\n🗺️ Kreuzberg\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Friedrichshain\n\nSince 16:00 28.4.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1590674407323"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590688801808",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Möckernbrücke\n🚉 Tempelhof\n\nDistricts:\n🗺️ Mitte\n🗺️ Kreuzberg\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Friedrichshain\n\nSince 20:00 28.4.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1590688807372"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590703201887",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Möckernbrücke\n🚉 Tempelhof\n\nDistricts:\n🗺️ Mitte\n🗺️ Kreuzberg\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Friedrichshain\n\nSince 0:00 29.4.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1590703207125"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590717602070",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Möckernbrücke\n🚉 Tempelhof\n\nDistricts:\n🗺️ Mitte\n🗺️ Kreuzberg\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Friedrichshain\n\nSince 4:00 29.4.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1590717607314"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590732002151",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Möckernbrücke\n🚉 Tempelhof\n\nDistricts:\n🗺️ Mitte\n🗺️ Kreuzberg\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Friedrichshain\n\nSince 8:00 29.4.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1590732008162"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590746402114",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Möckernbrücke\n🚉 Tempelhof\n\nDistricts:\n🗺️ Mitte\n🗺️ Kreuzberg\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Friedrichshain\n\nSince 12:00 29.4.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1590746407507"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590760802280",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Möckernbrücke\n🚉 Tempelhof\n\nDistricts:\n🗺️ Mitte\n🗺️ Kreuzberg\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Friedrichshain\n\nSince 16:00 29.4.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1590760808035"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590775202338",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Möckernbrücke\n🚉 Tempelhof\n\nDistricts:\n🗺️ Mitte\n🗺️ Kreuzberg\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Friedrichshain\n\nSince 20:00 29.4.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1590775207812"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Brandenburger Tor",
          "id": "900000100025"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590789602410",
        "text": "Most controlled on weekend days:\n\nStations:\n🚉 Naturkundemuseum\n🚉 Brandenburger Tor\n🚉 Stadtmitte\n🚉 Kleistpark\n🚉 Kottbusser Tor\n\nDistricts:\n🗺️ Mitte\n🗺️ Tempelhof\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Prenzlauer Berg\n\nSince 0:00 30.4.2020"
      },
      "geometry": {
        "coordinates": [
          13.381937,
          52.516511
        ],
        "type": "Point"
      },
      "id": "1590789607768"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Brandenburger Tor",
          "id": "900000100025"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590804002307",
        "text": "Most controlled on weekend days:\n\nStations:\n🚉 Naturkundemuseum\n🚉 Brandenburger Tor\n🚉 Stadtmitte\n🚉 Kleistpark\n🚉 Kottbusser Tor\n\nDistricts:\n🗺️ Mitte\n🗺️ Tempelhof\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Prenzlauer Berg\n\nSince 4:00 30.4.2020"
      },
      "geometry": {
        "coordinates": [
          13.381937,
          52.516511
        ],
        "type": "Point"
      },
      "id": "1590804007614"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Brandenburger Tor",
          "id": "900000100025"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590818402408",
        "text": "Most controlled on weekend days:\n\nStations:\n🚉 Naturkundemuseum\n🚉 Brandenburger Tor\n🚉 Stadtmitte\n🚉 Kleistpark\n🚉 Kottbusser Tor\n\nDistricts:\n🗺️ Mitte\n🗺️ Tempelhof\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Prenzlauer Berg\n\nSince 8:00 30.4.2020"
      },
      "geometry": {
        "coordinates": [
          13.381937,
          52.516511
        ],
        "type": "Point"
      },
      "id": "1590818407732"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Brandenburger Tor",
          "id": "900000100025"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590832802402",
        "text": "Most controlled on weekend days:\n\nStations:\n🚉 Naturkundemuseum\n🚉 Brandenburger Tor\n🚉 Stadtmitte\n🚉 Kleistpark\n🚉 Kottbusser Tor\n\nDistricts:\n🗺️ Mitte\n🗺️ Tempelhof\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Prenzlauer Berg\n\nSince 12:00 30.4.2020"
      },
      "geometry": {
        "coordinates": [
          13.381937,
          52.516511
        ],
        "type": "Point"
      },
      "id": "1590832807749"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Brandenburger Tor",
          "id": "900000100025"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590847202491",
        "text": "Most controlled on weekend days:\n\nStations:\n🚉 Naturkundemuseum\n🚉 Brandenburger Tor\n🚉 Stadtmitte\n🚉 Kleistpark\n🚉 Kottbusser Tor\n\nDistricts:\n🗺️ Mitte\n🗺️ Tempelhof\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Prenzlauer Berg\n\nSince 16:00 30.4.2020"
      },
      "geometry": {
        "coordinates": [
          13.381937,
          52.516511
        ],
        "type": "Point"
      },
      "id": "1590847207963"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Brandenburger Tor",
          "id": "900000100025"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590861602577",
        "text": "Most controlled on weekend days:\n\nStations:\n🚉 Naturkundemuseum\n🚉 Brandenburger Tor\n🚉 Stadtmitte\n🚉 Kleistpark\n🚉 Kottbusser Tor\n\nDistricts:\n🗺️ Mitte\n🗺️ Tempelhof\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Prenzlauer Berg\n\nSince 20:00 30.4.2020"
      },
      "geometry": {
        "coordinates": [
          13.381937,
          52.516511
        ],
        "type": "Point"
      },
      "id": "1590861607989"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Brandenburger Tor",
          "id": "900000100025"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590876002668",
        "text": "Most controlled on weekend days:\n\nStations:\n🚉 Naturkundemuseum\n🚉 Brandenburger Tor\n🚉 Stadtmitte\n🚉 Kleistpark\n🚉 Kottbusser Tor\n\nDistricts:\n🗺️ Mitte\n🗺️ Tempelhof\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Prenzlauer Berg\n\nSince 0:00 31.4.2020"
      },
      "geometry": {
        "coordinates": [
          13.381937,
          52.516511
        ],
        "type": "Point"
      },
      "id": "1590876007904"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Brandenburger Tor",
          "id": "900000100025"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590890402704",
        "text": "Most controlled on weekend days:\n\nStations:\n🚉 Naturkundemuseum\n🚉 Brandenburger Tor\n🚉 Stadtmitte\n🚉 Kleistpark\n🚉 Kottbusser Tor\n\nDistricts:\n🗺️ Mitte\n🗺️ Tempelhof\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Prenzlauer Berg\n\nSince 4:00 31.4.2020"
      },
      "geometry": {
        "coordinates": [
          13.381937,
          52.516511
        ],
        "type": "Point"
      },
      "id": "1590890408042"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Brandenburger Tor",
          "id": "900000100025"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590904802885",
        "text": "Most controlled on weekend days:\n\nStations:\n🚉 Naturkundemuseum\n🚉 Brandenburger Tor\n🚉 Stadtmitte\n🚉 Kleistpark\n🚉 Kottbusser Tor\n\nDistricts:\n🗺️ Mitte\n🗺️ Tempelhof\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Prenzlauer Berg\n\nSince 8:00 31.4.2020"
      },
      "geometry": {
        "coordinates": [
          13.381937,
          52.516511
        ],
        "type": "Point"
      },
      "id": "1590904808133"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Brandenburger Tor",
          "id": "900000100025"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590919202892",
        "text": "Most controlled on weekend days:\n\nStations:\n🚉 Naturkundemuseum\n🚉 Brandenburger Tor\n🚉 Stadtmitte\n🚉 Kleistpark\n🚉 Kottbusser Tor\n\nDistricts:\n🗺️ Mitte\n🗺️ Tempelhof\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Prenzlauer Berg\n\nSince 12:00 31.4.2020"
      },
      "geometry": {
        "coordinates": [
          13.381937,
          52.516511
        ],
        "type": "Point"
      },
      "id": "1590919208149"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Brandenburger Tor",
          "id": "900000100025"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590933603017",
        "text": "Most controlled on weekend days:\n\nStations:\n🚉 Naturkundemuseum\n🚉 Brandenburger Tor\n🚉 Stadtmitte\n🚉 Kleistpark\n🚉 Kottbusser Tor\n\nDistricts:\n🗺️ Mitte\n🗺️ Tempelhof\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Prenzlauer Berg\n\nSince 16:00 31.4.2020"
      },
      "geometry": {
        "coordinates": [
          13.381937,
          52.516511
        ],
        "type": "Point"
      },
      "id": "1590933608604"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Brandenburger Tor",
          "id": "900000100025"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590948003189",
        "text": "Most controlled on weekend days:\n\nStations:\n🚉 Naturkundemuseum\n🚉 Brandenburger Tor\n🚉 Stadtmitte\n🚉 Kleistpark\n🚉 Kottbusser Tor\n\nDistricts:\n🗺️ Mitte\n🗺️ Tempelhof\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Prenzlauer Berg\n\nSince 20:00 31.4.2020"
      },
      "geometry": {
        "coordinates": [
          13.381937,
          52.516511
        ],
        "type": "Point"
      },
      "id": "1590948008658"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1590962403267",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Tempelhof\n\nDistricts:\n🗺️ Mitte\n🗺️ Kreuzberg\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Friedrichshain\n\nSince 0:00 1.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1590962408551"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1591178404373",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Tempelhof\n\nDistricts:\n🗺️ Mitte\n🗺️ Kreuzberg\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Friedrichshain\n\nSince 12:00 3.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1591178409492"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1591192804470",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Tempelhof\n\nDistricts:\n🗺️ Mitte\n🗺️ Kreuzberg\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Friedrichshain\n\nSince 16:00 3.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1591192810156"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1591207204583",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Tempelhof\n\nDistricts:\n🗺️ Mitte\n🗺️ Kreuzberg\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Friedrichshain\n\nSince 20:00 3.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1591207209833"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1591610406703",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Charlottenburg\n\nSince 12:00 8.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1591610411971"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1591639206826",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Charlottenburg\n\nSince 20:00 8.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1591639212188"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1591653606907",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Charlottenburg\n\nSince 0:00 9.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1591653612142"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1591668006942",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Charlottenburg\n\nSince 4:00 9.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1591668012203"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1591682406961",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Charlottenburg\n\nSince 8:00 9.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1591682412202"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1591696807105",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Charlottenburg\n\nSince 12:00 9.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1591696812387"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1591711207185",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Charlottenburg\n\nSince 16:00 9.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1591711212467"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1591711566639",
        "text": "RT @avoidcontrol: Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDi…"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1591711621796"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1591725607247",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Charlottenburg\n\nSince 20:00 9.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1591725612555"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1591740007342",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Charlottenburg\n\nSince 0:00 10.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1591740012680"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1591754407388",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Charlottenburg\n\nSince 4:00 10.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1591754412672"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1591768807408",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Charlottenburg\n\nSince 8:00 10.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1591768812671"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1591783207499",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Charlottenburg\n\nSince 12:00 10.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1591783212714"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1591797607644",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Charlottenburg\n\nSince 16:00 10.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1591797613014"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1591812007737",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Charlottenburg\n\nSince 20:00 10.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1591812013038"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1591826407727",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Charlottenburg\n\nSince 0:00 11.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1591826413029"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1591840807779",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Charlottenburg\n\nSince 4:00 11.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1591840813026"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1591855207884",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Charlottenburg\n\nSince 8:00 11.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1591855213128"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1591869607875",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Charlottenburg\n\nSince 12:00 11.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1591869613118"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1591884007970",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Charlottenburg\n\nSince 16:00 11.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1591884013270"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1591898408047",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Charlottenburg\n\nSince 20:00 11.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1591898414821"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1591912808069",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Charlottenburg\n\nSince 0:00 12.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1591912813317"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Brandenburger Tor",
          "id": "900000100025"
        },
        "direction": null,
        "line": null,
        "timestamp": "1592042408609",
        "text": "Most controlled on weekend days:\n\nStations:\n🚉 Naturkundemuseum\n🚉 Brandenburger Tor\n🚉 Stadtmitte\n🚉 Kleistpark\n🚉 Kottbusser Tor\n\nDistricts:\n🗺️ Mitte\n🗺️ Tempelhof\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Prenzlauer Berg\n\nSince 12:00 13.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.381937,
          52.516511
        ],
        "type": "Point"
      },
      "id": "1592042413860"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Brandenburger Tor",
          "id": "900000100025"
        },
        "direction": null,
        "line": null,
        "timestamp": "1592056808646",
        "text": "Most controlled on weekend days:\n\nStations:\n🚉 Naturkundemuseum\n🚉 Brandenburger Tor\n🚉 Stadtmitte\n🚉 Kleistpark\n🚉 Kottbusser Tor\n\nDistricts:\n🗺️ Mitte\n🗺️ Tempelhof\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Prenzlauer Berg\n\nSince 16:00 13.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.381937,
          52.516511
        ],
        "type": "Point"
      },
      "id": "1592056813961"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Brandenburger Tor",
          "id": "900000100025"
        },
        "direction": null,
        "line": null,
        "timestamp": "1592071208774",
        "text": "Most controlled on weekend days:\n\nStations:\n🚉 Naturkundemuseum\n🚉 Brandenburger Tor\n🚉 Stadtmitte\n🚉 Kleistpark\n🚉 Kottbusser Tor\n\nDistricts:\n🗺️ Mitte\n🗺️ Tempelhof\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Prenzlauer Berg\n\nSince 20:00 13.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.381937,
          52.516511
        ],
        "type": "Point"
      },
      "id": "1592071214021"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Brandenburger Tor",
          "id": "900000100025"
        },
        "direction": null,
        "line": null,
        "timestamp": "1592085608766",
        "text": "Most controlled on weekend days:\n\nStations:\n🚉 Naturkundemuseum\n🚉 Brandenburger Tor\n🚉 Stadtmitte\n🚉 Kleistpark\n🚉 Kottbusser Tor\n\nDistricts:\n🗺️ Mitte\n🗺️ Tempelhof\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Prenzlauer Berg\n\nSince 0:00 14.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.381937,
          52.516511
        ],
        "type": "Point"
      },
      "id": "1592085614104"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1592244009621",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 20:00 15.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1592244014933"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1592258409706",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 0:00 16.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1592258415057"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1592560811420",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 12:00 19.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1592560816650"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1592575211581",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 16:00 19.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1592575216813"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1592589611559",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 20:00 19.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1592589616786"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Brandenburger Tor",
          "id": "900000100025"
        },
        "direction": null,
        "line": null,
        "timestamp": "1592604011550",
        "text": "Most controlled on weekend days:\n\nStations:\n🚉 Naturkundemuseum\n🚉 Brandenburger Tor\n🚉 Stadtmitte\n🚉 Kleistpark\n🚉 Kottbusser Tor\n\nDistricts:\n🗺️ Mitte\n🗺️ Tempelhof\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Prenzlauer Berg\n\nSince 0:00 20.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.381937,
          52.516511
        ],
        "type": "Point"
      },
      "id": "1592604016802"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1592906413135",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 12:00 23.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1592906418372"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1592920813175",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 16:00 23.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1592920818551"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1592935213278",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 20:00 23.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1592935218562"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1592949613423",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 0:00 24.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1592949618792"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1592992813629",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 12:00 24.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1592992818881"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1593007213695",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 16:00 24.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1593007218989"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1593021613828",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 20:00 24.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1593021619124"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1593036013886",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 0:00 25.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1593036070730"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1593424816174",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 12:00 29.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1593424821524"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1593439216273",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 16:00 29.5.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1593439221934"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1593684017451",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 12:00 2.6.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1593684022689"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1594029619255",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 12:00 6.6.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1594029624483"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1594044019268",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 16:00 6.6.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1594044024512"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1594116019708",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 12:00 7.6.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1594116025045"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1594648822256",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 16:00 13.6.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1594648827458"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1594663222404",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 20:00 13.6.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1594663227663"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1594677622489",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 0:00 14.6.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1594677627774"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1594692022517",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 4:00 14.6.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1594692027789"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1594706422638",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 8:00 14.6.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1594706427903"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1594720822616",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 12:00 14.6.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1594720827924"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1594735222687",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 16:00 14.6.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1594735227962"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1594807223033",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 12:00 15.6.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1594807228329"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1595887228323",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 0:00 28.6.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1595887233657"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1595901628476",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 4:00 28.6.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1595901633675"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1595916028497",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 8:00 28.6.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1595916033778"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1595959228698",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 20:00 28.6.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1595959233976"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1595973628689",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 0:00 29.6.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1595973633939"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1595988028770",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 4:00 29.6.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1595988034018"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1596002428839",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 8:00 29.6.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1596002434087"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1596016828951",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 12:00 29.6.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1596016834220"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1596031228950",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 16:00 29.6.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1596031234288"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1596060029105",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 0:00 30.6.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1596060034398"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1596074430193",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 4:00 30.6.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1596074435439"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1596103229722",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 12:00 30.6.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1596103234946"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1596117629497",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 16:00 30.6.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1596117634768"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1596132029527",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 20:00 30.6.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1596132034751"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1596146429731",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 0:00 31.6.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1596146434953"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1596160829704",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 4:00 31.6.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1596160834921"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1596175229777",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 8:00 31.6.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1596175235024"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1596189629871",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 12:00 31.6.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1596189635124"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1596204029979",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 16:00 31.6.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1596204035180"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1596218430024",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 20:00 31.6.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1596218435316"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Brandenburger Tor",
          "id": "900000100025"
        },
        "direction": null,
        "line": null,
        "timestamp": "1596232830027",
        "text": "Most controlled on weekend days:\n\nStations:\n🚉 Naturkundemuseum\n🚉 Brandenburger Tor\n🚉 Stadtmitte\n🚉 Kleistpark\n🚉 Kottbusser Tor\n\nDistricts:\n🗺️ Mitte\n🗺️ Tempelhof\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Prenzlauer Berg\n\nSince 0:00 1.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.381937,
          52.516511
        ],
        "type": "Point"
      },
      "id": "1596232835334"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Brandenburger Tor",
          "id": "900000100025"
        },
        "direction": null,
        "line": null,
        "timestamp": "1596247230098",
        "text": "Most controlled on weekend days:\n\nStations:\n🚉 Naturkundemuseum\n🚉 Brandenburger Tor\n🚉 Stadtmitte\n🚉 Kleistpark\n🚉 Kottbusser Tor\n\nDistricts:\n🗺️ Mitte\n🗺️ Tempelhof\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Prenzlauer Berg\n\nSince 4:00 1.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.381937,
          52.516511
        ],
        "type": "Point"
      },
      "id": "1596247235380"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Brandenburger Tor",
          "id": "900000100025"
        },
        "direction": null,
        "line": null,
        "timestamp": "1596261630139",
        "text": "Most controlled on weekend days:\n\nStations:\n🚉 Naturkundemuseum\n🚉 Brandenburger Tor\n🚉 Stadtmitte\n🚉 Kleistpark\n🚉 Kottbusser Tor\n\nDistricts:\n🗺️ Mitte\n🗺️ Tempelhof\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Prenzlauer Berg\n\nSince 8:00 1.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.381937,
          52.516511
        ],
        "type": "Point"
      },
      "id": "1596261635379"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Brandenburger Tor",
          "id": "900000100025"
        },
        "direction": null,
        "line": null,
        "timestamp": "1596276030214",
        "text": "Most controlled on weekend days:\n\nStations:\n🚉 Naturkundemuseum\n🚉 Brandenburger Tor\n🚉 Stadtmitte\n🚉 Kleistpark\n🚉 Kottbusser Tor\n\nDistricts:\n🗺️ Mitte\n🗺️ Tempelhof\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Prenzlauer Berg\n\nSince 12:00 1.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.381937,
          52.516511
        ],
        "type": "Point"
      },
      "id": "1596276035443"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Brandenburger Tor",
          "id": "900000100025"
        },
        "direction": null,
        "line": null,
        "timestamp": "1596290430289",
        "text": "Most controlled on weekend days:\n\nStations:\n🚉 Naturkundemuseum\n🚉 Brandenburger Tor\n🚉 Stadtmitte\n🚉 Kleistpark\n🚉 Kottbusser Tor\n\nDistricts:\n🗺️ Mitte\n🗺️ Tempelhof\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Prenzlauer Berg\n\nSince 16:00 1.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.381937,
          52.516511
        ],
        "type": "Point"
      },
      "id": "1596290435487"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Brandenburger Tor",
          "id": "900000100025"
        },
        "direction": null,
        "line": null,
        "timestamp": "1596304830325",
        "text": "Most controlled on weekend days:\n\nStations:\n🚉 Naturkundemuseum\n🚉 Brandenburger Tor\n🚉 Stadtmitte\n🚉 Kleistpark\n🚉 Kottbusser Tor\n\nDistricts:\n🗺️ Mitte\n🗺️ Tempelhof\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Prenzlauer Berg\n\nSince 20:00 1.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.381937,
          52.516511
        ],
        "type": "Point"
      },
      "id": "1596304835550"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Brandenburger Tor",
          "id": "900000100025"
        },
        "direction": null,
        "line": null,
        "timestamp": "1596319230485",
        "text": "Most controlled on weekend days:\n\nStations:\n🚉 Naturkundemuseum\n🚉 Brandenburger Tor\n🚉 Stadtmitte\n🚉 Kleistpark\n🚉 Kottbusser Tor\n\nDistricts:\n🗺️ Mitte\n🗺️ Tempelhof\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Prenzlauer Berg\n\nSince 0:00 2.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.381937,
          52.516511
        ],
        "type": "Point"
      },
      "id": "1596319235714"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Brandenburger Tor",
          "id": "900000100025"
        },
        "direction": null,
        "line": null,
        "timestamp": "1596333630569",
        "text": "Most controlled on weekend days:\n\nStations:\n🚉 Naturkundemuseum\n🚉 Brandenburger Tor\n🚉 Stadtmitte\n🚉 Kleistpark\n🚉 Kottbusser Tor\n\nDistricts:\n🗺️ Mitte\n🗺️ Tempelhof\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Prenzlauer Berg\n\nSince 4:00 2.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.381937,
          52.516511
        ],
        "type": "Point"
      },
      "id": "1596333635801"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Brandenburger Tor",
          "id": "900000100025"
        },
        "direction": null,
        "line": null,
        "timestamp": "1596348030575",
        "text": "Most controlled on weekend days:\n\nStations:\n🚉 Naturkundemuseum\n🚉 Brandenburger Tor\n🚉 Stadtmitte\n🚉 Kleistpark\n🚉 Kottbusser Tor\n\nDistricts:\n🗺️ Mitte\n🗺️ Tempelhof\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Prenzlauer Berg\n\nSince 8:00 2.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.381937,
          52.516511
        ],
        "type": "Point"
      },
      "id": "1596348035772"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Brandenburger Tor",
          "id": "900000100025"
        },
        "direction": null,
        "line": null,
        "timestamp": "1596362430717",
        "text": "Most controlled on weekend days:\n\nStations:\n🚉 Naturkundemuseum\n🚉 Brandenburger Tor\n🚉 Stadtmitte\n🚉 Kleistpark\n🚉 Kottbusser Tor\n\nDistricts:\n🗺️ Mitte\n🗺️ Tempelhof\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Prenzlauer Berg\n\nSince 12:00 2.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.381937,
          52.516511
        ],
        "type": "Point"
      },
      "id": "1596362435938"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Brandenburger Tor",
          "id": "900000100025"
        },
        "direction": null,
        "line": null,
        "timestamp": "1596376830720",
        "text": "Most controlled on weekend days:\n\nStations:\n🚉 Naturkundemuseum\n🚉 Brandenburger Tor\n🚉 Stadtmitte\n🚉 Kleistpark\n🚉 Kottbusser Tor\n\nDistricts:\n🗺️ Mitte\n🗺️ Tempelhof\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Prenzlauer Berg\n\nSince 16:00 2.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.381937,
          52.516511
        ],
        "type": "Point"
      },
      "id": "1596376836031"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Brandenburger Tor",
          "id": "900000100025"
        },
        "direction": null,
        "line": null,
        "timestamp": "1596391230799",
        "text": "Most controlled on weekend days:\n\nStations:\n🚉 Naturkundemuseum\n🚉 Brandenburger Tor\n🚉 Stadtmitte\n🚉 Kleistpark\n🚉 Kottbusser Tor\n\nDistricts:\n🗺️ Mitte\n🗺️ Tempelhof\n🗺️ Schöneberg\n🗺️ Charlottenburg\n🗺️ Prenzlauer Berg\n\nSince 20:00 2.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.381937,
          52.516511
        ],
        "type": "Point"
      },
      "id": "1596391236098"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1596405631070",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 0:00 3.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1596405636338"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1596420031108",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 4:00 3.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1596420036312"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1596434431205",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 8:00 3.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1596434436426"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1596448831258",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 12:00 3.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1596448836491"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1596578431978",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 0:00 5.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1596578437253"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1596592832077",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 4:00 5.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1596592837378"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1596607232145",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 8:00 5.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1596607237403"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1596621632270",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 12:00 5.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1596621637651"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1598292040721",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 20:00 24.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1598292045987"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1598306440849",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 0:00 25.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1598306446081"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1598320840896",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 4:00 25.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1598320846179"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1598335240983",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 8:00 25.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1598335246256"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1598349641131",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 12:00 25.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1598349646346"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1598364041194",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 16:00 25.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1598364046513"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1598378441229",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 20:00 25.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1598378446550"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1598392841300",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 0:00 26.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1598392846612"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1598407241364",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 4:00 26.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1598407246623"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1598421641522",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 8:00 26.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1598421646791"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1598436041535",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 12:00 26.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1598436046810"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1598450441548",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 16:00 26.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1598450446822"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1598464841674",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 20:00 26.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1598464846964"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1598479241716",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 0:00 27.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1598479246989"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1598493641770",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 4:00 27.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1598493646990"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1598508041840",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 8:00 27.7.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1598508047130"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1599084045134",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 0:00 3.8.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1599084050449"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1599098445271",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 4:00 3.8.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1599098450505"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1599112845284",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 8:00 3.8.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1599112850509"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1599127245468",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 12:00 3.8.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1599127250667"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1599141645484",
        "text": "Most controlled on week days:\n\nStations:\n🚉 Alexanderplatz\n🚉 Möckernbrücke\n🚉 Berlin Hauptbahnhof\n🚉 Bellevue\n🚉 Südkreuz\n\nDistricts:\n🗺️ Mitte\n🗺️ Schöneberg\n🗺️ Kreuzberg\n🗺️ Prenzlauer Berg\n🗺️ Neukölln\n\nSince 16:00 3.8.2020"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1599141650811"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Naturkundemuseum",
          "id": "900000100009"
        },
        "direction": null,
        "line": {
          "name": "U6",
          "id": null
        },
        "timestamp": "1496079956000",
        "text": "RT @NickPower3939: @ber_ohne_ticket Kontrolle #u6 bei Naturkundemuseum in der nähe"
      },
      "geometry": {
        "coordinates": [
          13.382415,
          52.531254
        ],
        "type": "Point"
      },
      "id": "164ae88c9e2636945aedc910d9f15c30"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Heinrich-Heine-Str",
          "id": "900000100008"
        },
        "direction": null,
        "line": {
          "name": "U8",
          "id": null
        },
        "timestamp": "1579698018000",
        "text": "RT @hackosauri: #noticket_bln 4 männliche* Kontorlettis auf der Linie U8 momentan Heinrich-Heine-Str. Einer mit Armeemuster Mütze, einer mi…"
      },
      "geometry": {
        "coordinates": [
          13.416169,
          52.510858
        ],
        "type": "Point"
      },
      "id": "17445491f028f5bef955ce7e6a0d1eda"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Rosenthaler Platz",
          "id": "900000100023"
        },
        "direction": null,
        "line": null,
        "timestamp": "1497597938000",
        "text": "RT @dumbkaido: @ber_ohne_ticket #kontrolle Rosenthaler Platz mit Polizei."
      },
      "geometry": {
        "coordinates": [
          13.401393,
          52.529781
        ],
        "type": "Point"
      },
      "id": "17b8ff1a0348a526489716046f2fd297"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Rosenthaler Platz",
          "id": "900000100023"
        },
        "direction": null,
        "line": null,
        "timestamp": "1492500036000",
        "text": "RT @dumbkaido: @ber_ohne_ticket #kontrolle gerade Rosenthaler Platz ausgestiegen."
      },
      "geometry": {
        "coordinates": [
          13.401393,
          52.529781
        ],
        "type": "Point"
      },
      "id": "1a096974e4cc473dbf8496d0ab53a317"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1580485849000",
        "text": "RT @annalist: Aktuell Kontrollen auf der Stadtbahn ab Hauptbahnhof Richtung Potsdam #noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "1a0e2efc5c3653e9b212a799149fe2d4"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Ahrensfelde",
          "id": "900000170004"
        },
        "direction": null,
        "line": {
          "name": "S7",
          "id": null
        },
        "timestamp": "1581592190000",
        "text": "RT @saytine: kontrolle s7 ri ahrensfelde, jetzt hauptbahnhof ausgestiegen, @noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.566118,
          52.571564
        ],
        "type": "Point"
      },
      "id": "1a9ccddadc67360ef3389376d999fd78"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Westkreuz",
          "id": "900000024102"
        },
        "direction": null,
        "line": {
          "name": "S7",
          "id": null
        },
        "timestamp": "1580209513000",
        "text": "RT @nachhutt: Fahrscheinkontrolle in der S7 Richtung Potsdam auf Höhe Westkreuz. Ein Mann, schwarze Haare, schwarze Fleece-Jacke und graue…"
      },
      "geometry": {
        "coordinates": [
          13.283036,
          52.501152
        ],
        "type": "Point"
      },
      "id": "1af11b797af404865d3f733f5ad7272a"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Potsdamer Platz",
          "id": "900000100020"
        },
        "direction": null,
        "line": null,
        "timestamp": "1582736105000",
        "text": "RT @easytarget2000: @noticket_bln Kontrolle in der S-Bahn auf Höhe Potsdamer Platz. Einer der Kontrolleure mit Jack Wolfskin Bauchtasche"
      },
      "geometry": {
        "coordinates": [
          13.376454,
          52.50934
        ],
        "type": "Point"
      },
      "id": "1c2410be084cb17e30d97e01414b2481"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Rosenthaler Platz",
          "id": "900000100023"
        },
        "direction": null,
        "line": {
          "name": "U8",
          "id": null
        },
        "timestamp": "1492586445000",
        "text": "RT @dumbkaido: @ber_ohne_ticket #kontrolle U8, gerade bei Rosenthaler Platz ausgestiegen."
      },
      "geometry": {
        "coordinates": [
          13.401393,
          52.529781
        ],
        "type": "Point"
      },
      "id": "1e79d1c1c57b3585286a55cded7f7fbc"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Frankfurter Tor",
          "id": "900000120008"
        },
        "direction": {
          "name": "Warschauer Str",
          "id": "900000120004"
        },
        "line": {
          "name": "M10",
          "id": null
        },
        "timestamp": "1528270452000",
        "text": "RT @Zeitfixierer: Kontrolle: M10 Frankfurter Tor in Richtung Warschauer Str. @ber_ohne_ticket #kontrolle #dieMachenDochAuchNurIhrenJob #Arm…"
      },
      "geometry": {
        "coordinates": [
          13.454085,
          52.515772
        ],
        "type": "Point"
      },
      "id": "1fafb37d54b59dde2159fe158ea101ce"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Rosenthaler Platz",
          "id": "900000100023"
        },
        "direction": null,
        "line": {
          "name": "U8",
          "id": null
        },
        "timestamp": "1494314465000",
        "text": "RT @dumbkaido: @ber_ohne_ticket #kontrolle U8, gerade Rosenthaler Platz ausgestiegen."
      },
      "geometry": {
        "coordinates": [
          13.401393,
          52.529781
        ],
        "type": "Point"
      },
      "id": "2006fc7c63037775f0f2eb1f95ddcb56"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Landsberger Allee",
          "id": "900000110004"
        },
        "direction": {
          "name": "Ostkreuz",
          "id": "900000120003"
        },
        "line": {
          "name": "S41",
          "id": null
        },
        "timestamp": "1581114370000",
        "text": "RT @Frau_Koenig: 23:24 Landsberger Allee S41 Richtung Ostkreuz. 2 männlich gelesene. Einer ganz in Schwarz, einer mit blauer Jeans und schw…"
      },
      "geometry": {
        "coordinates": [
          13.455944,
          52.528772
        ],
        "type": "Point"
      },
      "id": "20dbbb43f194b3ea23426fe7cbba6e36"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Kurfürstenstr",
          "id": "900000005201"
        },
        "direction": null,
        "line": {
          "name": "U1",
          "id": null
        },
        "timestamp": "1575882939000",
        "text": "RT @KOstberl: #Kontrolle U1 Höhe Kurfürstenstr 2Typen 1 Frau. Ein Typ kräftig mit Palituch 10:12h #noticket_bln @noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.362814,
          52.49981
        ],
        "type": "Point"
      },
      "id": "230c9235e9eeb36f0b70dbc1cab0ee07"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Schöneweide",
          "id": "900000192001"
        },
        "direction": null,
        "line": {
          "name": "S46",
          "id": null
        },
        "timestamp": "1576133317000",
        "text": "RT @ArthurPobel: #noticket_bln\nKontrolle S46 - Westend\nZwischen Schöneweide und Baumschulenweg"
      },
      "geometry": {
        "coordinates": [
          13.510149,
          52.454611
        ],
        "type": "Point"
      },
      "id": "2454f42a6f949336fc5b0acdb92a2db5"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Lichterfelde Ost",
          "id": "900000064301"
        },
        "direction": null,
        "line": {
          "name": "S2",
          "id": null
        },
        "timestamp": "1576564514000",
        "text": "RT @lysalia_: @noticket_bln 07:33\nS26 teltow\nGerade an s lichterfelde ost.\n2 kontrolleure\nBeide in schwarz\nEiner hat einen dunkelroten deut…"
      },
      "geometry": {
        "coordinates": [
          13.328078,
          52.429098
        ],
        "type": "Point"
      },
      "id": "29b88859814ae72a23c1a81c0a71f305"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Alexanderplatz",
          "id": "900000100003"
        },
        "direction": null,
        "line": {
          "name": "U8",
          "id": null
        },
        "timestamp": "1479802584000",
        "text": "RT @dumbkaido: #kontrolle U8 im Bahnhof Alexanderplatz. Kontrolleur*innen und Polizist*innen. @ber_ohne_ticket"
      },
      "geometry": {
        "coordinates": [
          13.411267,
          52.521512
        ],
        "type": "Point"
      },
      "id": "2c3425e9a74f7eb00f193881f3aae380"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Bellevue",
          "id": "900000003102"
        },
        "direction": {
          "name": "Ahrensfelde",
          "id": "900000170004"
        },
        "line": {
          "name": "S7",
          "id": null
        },
        "timestamp": "1579614100000",
        "text": "RT @aluhutt: Fahrscheinkontrolle auf der Stadtbahnstrecke (S7) Richtung Ahrensfelde; gerade an der S Bellevue: zwei Männer, einer mit grüne…"
      },
      "geometry": {
        "coordinates": [
          13.347098,
          52.519951
        ],
        "type": "Point"
      },
      "id": "319043f6b3d586db26be803aeff331c5"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Hönow",
          "id": "900000175010"
        },
        "direction": null,
        "line": {
          "name": "U5",
          "id": null
        },
        "timestamp": "1579159215000",
        "text": "RT @Jaysmithjs: U5 elstawerdaer platz ri hönow wechseln die waggons an jeder station. 2 dudes #noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.634541,
          52.538105
        ],
        "type": "Point"
      },
      "id": "31de531f2a02b1000db7dc97def396fa"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Birkenstein",
          "id": "900000320026"
        },
        "direction": {
          "name": "Strausberg",
          "id": "900000320004"
        },
        "line": {
          "name": "S2",
          "id": null
        },
        "timestamp": "1582628400000",
        "text": "RT @HumRights24_7: @noticket_bln S5 Richtung Strausberg. Steigen gerade in Birkenstein ein. Ein Mann mit Jeans und eine Frau mit schwarzer…"
      },
      "geometry": {
        "coordinates": [
          13.648591,
          52.515755
        ],
        "type": "Point"
      },
      "id": "31e4c6a365d6a3e2d4c59cfb27205a4b"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Bismarckstr",
          "id": "900000024201"
        },
        "direction": null,
        "line": {
          "name": "U7",
          "id": null
        },
        "timestamp": "1573754965000",
        "text": "RT @rummelboxxer: #noticket_bln U7 bhf Bismarckstraße junger Typ mit heller jacke"
      },
      "geometry": {
        "coordinates": [
          13.305286,
          52.511513
        ],
        "type": "Point"
      },
      "id": "32e4fd6858b9362dc9437892bead72f8"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Friedrichstr",
          "id": "900000100001"
        },
        "direction": {
          "name": "Nöldnerplatz",
          "id": "900000160003"
        },
        "line": {
          "name": "S7",
          "id": null
        },
        "timestamp": "1572367223000",
        "text": "RT @ArthurPobel: @noticket_bln #noticket_bln\nS7 richtung nöldnerplatz höhe Friedrichstraße"
      },
      "geometry": {
        "coordinates": [
          13.387153,
          52.52027
        ],
        "type": "Point"
      },
      "id": "33ec6e8daadde6e869ad56e8ac92bc2a"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Weinmeisterstr",
          "id": "900000100051"
        },
        "direction": {
          "name": "Hermannstr",
          "id": "900000079221"
        },
        "line": {
          "name": "U8",
          "id": null
        },
        "timestamp": "1496819733000",
        "text": "RT @dumbkaido: @ber_ohne_ticket #kontrolle U8 Richtung Hermannstraße, gerade Weinmeisterstraße."
      },
      "geometry": {
        "coordinates": [
          13.405305,
          52.525376
        ],
        "type": "Point"
      },
      "id": "33ff86414f2222ef389728f20a420495"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Rosenthaler Platz",
          "id": "900000100023"
        },
        "direction": null,
        "line": {
          "name": "U8",
          "id": null
        },
        "timestamp": "1496301609000",
        "text": "RT @dumbkaido: @ber_ohne_ticket #kontrolle U8, gerade Rosenthaler Platz, 2 Männer*."
      },
      "geometry": {
        "coordinates": [
          13.401393,
          52.529781
        ],
        "type": "Point"
      },
      "id": "354dba5e6c0f72554d5f48a94114e219"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Yorckstr",
          "id": "900000057103"
        },
        "direction": {
          "name": "Rudow",
          "id": "900000083201"
        },
        "line": {
          "name": "U7",
          "id": null
        },
        "timestamp": "1576856126000",
        "text": "RT @smsamsa: @noticket_bln Kontrolle u7 yorckstrasse denke gleich Richtung rudow ...einer mit rot weißen Sportschuhe und basecap"
      },
      "geometry": {
        "coordinates": [
          13.370429,
          52.492766
        ],
        "type": "Point"
      },
      "id": "3607d27aa2591e2c9e8c4c6c825d8149"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Bellevue",
          "id": "900000003102"
        },
        "direction": null,
        "line": null,
        "timestamp": "1475571295000",
        "text": "RT @NickPower3939: @ber_ohne_ticket kontrolle bellevue und hauptbahnhof"
      },
      "geometry": {
        "coordinates": [
          13.347098,
          52.519951
        ],
        "type": "Point"
      },
      "id": "3642e57ec668407102e771af53c6ad0e"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Prenzlauer Allee",
          "id": "900000110002"
        },
        "direction": null,
        "line": {
          "name": "M13",
          "id": null
        },
        "timestamp": "1582617099000",
        "text": "RT @devfoo: @noticket_bln #kontrolle M13 Ri. Virchow, gerade ausgestiegen an Prenzlauer Allee / Ostseestraße"
      },
      "geometry": {
        "coordinates": [
          13.427419,
          52.544802
        ],
        "type": "Point"
      },
      "id": "36dc38b7773662f9137719db364c6e5f"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Görlitzer Bahnhof",
          "id": "900000014101"
        },
        "direction": null,
        "line": {
          "name": "U1",
          "id": null
        },
        "timestamp": "1484672780000",
        "text": "RT @dumbkaido: @ber_ohne_ticket #Kontrolle U1, gerade Görlitzer Bahnhof."
      },
      "geometry": {
        "coordinates": [
          13.428468,
          52.499035
        ],
        "type": "Point"
      },
      "id": "36eb2f2dfb7b888ec50d469a7837ab10"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Tempelhof",
          "id": "900000068201"
        },
        "direction": null,
        "line": {
          "name": "S42",
          "id": null
        },
        "timestamp": "1580289821000",
        "text": "RT @guatavita73: #noticket_bln Kontrolle jetzt Tempelhof s42"
      },
      "geometry": {
        "coordinates": [
          13.385754,
          52.470694
        ],
        "type": "Point"
      },
      "id": "38793b830d651628d2c265687f61b07c"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Tempelhof",
          "id": "900000068201"
        },
        "direction": null,
        "line": {
          "name": "S42",
          "id": null
        },
        "timestamp": "1571727744000",
        "text": "RT @_morre_: Zwei kontrollierende Menschen am S-Bahnhof Tempelhof, S42. Einer davon mit Jeansjacke und orangener Kapuze.\n\n#noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.385754,
          52.470694
        ],
        "type": "Point"
      },
      "id": "38b924afb1de2e79857f4672347391e6"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Zehlendorf",
          "id": "900000049201"
        },
        "direction": {
          "name": "Mexikoplatz",
          "id": "900000050301"
        },
        "line": {
          "name": "S1",
          "id": null
        },
        "timestamp": "1573823767000",
        "text": "RT @karloh_oh: @noticket_bln s1 Kontrolle Zehlendorf Richtung nmexikoplatz."
      },
      "geometry": {
        "coordinates": [
          13.259227,
          52.431209
        ],
        "type": "Point"
      },
      "id": "39598a1aa3e989eea41738dc81870296"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Tempelhof",
          "id": "900000068201"
        },
        "direction": null,
        "line": {
          "name": "S41",
          "id": null
        },
        "timestamp": "1459522562000",
        "text": "RT @NickPower3939: @ber_ohne_ticket #Fahrkartenkontrolle #s42 #s41 #s46 #s47 #s45  Zwischen #Tempelhof und #Hermannstraße."
      },
      "geometry": {
        "coordinates": [
          13.385754,
          52.470694
        ],
        "type": "Point"
      },
      "id": "39f738709b16ddfc91683c0503274b2a"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Schönhauser Allee",
          "id": "900000110001"
        },
        "direction": null,
        "line": {
          "name": "S41",
          "id": null
        },
        "timestamp": "1582303847000",
        "text": "RT @Schlafigel: Kontrolle s Bahn schönhauser Allee 2 Typen einer Mütze, Brille und Bart, anderer stepjacke roter und roter Schal. S41 #noti…"
      },
      "geometry": {
        "coordinates": [
          13.415138,
          52.549336
        ],
        "type": "Point"
      },
      "id": "3c0b972b853ee393b88d24a53b16942b"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": {
          "name": "Erkner",
          "id": "900000310004"
        },
        "line": {
          "name": "S3",
          "id": null
        },
        "timestamp": "1583426179000",
        "text": "RT @organicketa: #noticket_bln\nHauptbahnhof  s3 Richtung erkner \n2 Kontrolleure parka  dunkle kippt\nVollbart"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "3dbc65ea09b0e87cb9dcbcacaed20c8b"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Alexanderplatz",
          "id": "900000100003"
        },
        "direction": null,
        "line": null,
        "timestamp": "1582547186000",
        "text": "RT @_morre_: 1325 Kontrolle Alexanderplatz an der S-Bahn. #noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.411267,
          52.521512
        ],
        "type": "Point"
      },
      "id": "400c7392896b37211e8b38a4a8bb752b"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Gesundbrunnen",
          "id": "900000007102"
        },
        "direction": {
          "name": "Alexanderplatz",
          "id": "900000100003"
        },
        "line": {
          "name": "U8",
          "id": null
        },
        "timestamp": "1583916478000",
        "text": "RT @stadtprolet: U8 Gesundbrunnen 2 Frauen ein Mann, Richtung Alex @noticket_bln #noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.388372,
          52.548637
        ],
        "type": "Point"
      },
      "id": "401dcf59b8c5fd919dc7905a866c20b8"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Alexanderplatz",
          "id": "900000100003"
        },
        "direction": null,
        "line": {
          "name": "U2",
          "id": null
        },
        "timestamp": "1577357969000",
        "text": "RT @atari_terror: @noticket_bln U2 Alexander Platz"
      },
      "geometry": {
        "coordinates": [
          13.411267,
          52.521512
        ],
        "type": "Point"
      },
      "id": "4023fa4674ca2750a0936ce1113cf4a0"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Turmstr",
          "id": "900000003104"
        },
        "direction": null,
        "line": {
          "name": "U9",
          "id": null
        },
        "timestamp": "1484312240000",
        "text": "RT @NickPower3939: @ber_ohne_ticket #u9 #Turmstraße Großkontrolle! #kontrolle #ubahn"
      },
      "geometry": {
        "coordinates": [
          13.341417,
          52.525938
        ],
        "type": "Point"
      },
      "id": "46e4fe5f9892fcc7209493e3badf4566"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Alexanderplatz",
          "id": "900000100003"
        },
        "direction": {
          "name": "Wittenau",
          "id": "900000096101"
        },
        "line": {
          "name": "U8",
          "id": null
        },
        "timestamp": "1580996399000",
        "text": "RT @sasoufilou: Kontrolleure Alexanderplatz U8, Richtung Wittenau, sind gerade ausgestiegen. @noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.411267,
          52.521512
        ],
        "type": "Point"
      },
      "id": "4acbcb29c2cab15b45c7287b61782231"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Ruhleben",
          "id": "900000025202"
        },
        "direction": null,
        "line": {
          "name": "U2",
          "id": null
        },
        "timestamp": "1583847896000",
        "text": "RT @moti_luchim: U2 Rochtung Ruhleben, Klosterstr. ausgestiegen @noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.241902,
          52.525587
        ],
        "type": "Point"
      },
      "id": "4d535f8821c3d6299e4f817be85d5c71"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Brandenburger Tor",
          "id": "900000100025"
        },
        "direction": null,
        "line": {
          "name": "S2",
          "id": null
        },
        "timestamp": "1580304990000",
        "text": "RT @tpunktkpunkt: #noticket_bln S26 RichtungTeltow Stadt, jetzt Brandenburger Tor raus, mind. 4 Männer"
      },
      "geometry": {
        "coordinates": [
          13.381937,
          52.516511
        ],
        "type": "Point"
      },
      "id": "4dc71f500c59ecf2a302c0e7ccbaab3b"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Kleistpark",
          "id": "900000054102"
        },
        "direction": {
          "name": "Rudow",
          "id": "900000083201"
        },
        "line": {
          "name": "U7",
          "id": null
        },
        "timestamp": "1573398409000",
        "text": "RT @allesabreiszen: #Kontrolle U7 Richtung Rudow, sind Kleistpark ausgestiegen.\n#noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.360917,
          52.490845
        ],
        "type": "Point"
      },
      "id": "4fd5852e51b14b9975c5e154b77c33ec"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Weberwiese",
          "id": "900000120025"
        },
        "direction": {
          "name": "Hönow",
          "id": "900000175010"
        },
        "line": {
          "name": "U5",
          "id": null
        },
        "timestamp": "1577276782000",
        "text": "RT @milainspace: #noticket_bln u5 richtung hönow grad weberwiese"
      },
      "geometry": {
        "coordinates": [
          13.443278,
          52.516848
        ],
        "type": "Point"
      },
      "id": "507f51a6e883e75711dee8fe1faf4045"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Friedrichstr",
          "id": "900000100001"
        },
        "direction": null,
        "line": null,
        "timestamp": "1580914364000",
        "text": "RT @ngeisemeyer: Kontrolle auf der Stadtbahn zwischen Hbf und Friedrichstraße #noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.387153,
          52.52027
        ],
        "type": "Point"
      },
      "id": "513c7f3c1ccff5b625974c9c884fd13f"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Gleisdreieck",
          "id": "900000017103"
        },
        "direction": null,
        "line": {
          "name": "U1",
          "id": null
        },
        "timestamp": "1501857034000",
        "text": "RT @weakaside_: @ber_ohne_ticket U1 gleisdreieck zwei junge Männer mit undercut #kontrolle"
      },
      "geometry": {
        "coordinates": [
          13.374293,
          52.499587
        ],
        "type": "Point"
      },
      "id": "5569e5c022d17e2e7a96c113487b7c8f"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Ostbahnhof",
          "id": "900000120005"
        },
        "direction": null,
        "line": null,
        "timestamp": "1481810762000",
        "text": "RT @dumbkaido: @ber_ohne_ticket #kontrolle gerade beim Ostbahnhof ausgestiegen. 2 Männer*."
      },
      "geometry": {
        "coordinates": [
          13.435747,
          52.510001
        ],
        "type": "Point"
      },
      "id": "56de04d3dac92379940e72a52d96d3f4"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Möckernbrücke",
          "id": "900000017104"
        },
        "direction": {
          "name": "Spandau",
          "id": "900000029101"
        },
        "line": {
          "name": "U7",
          "id": null
        },
        "timestamp": "1578652342000",
        "text": "RT @ArthurPobel: #noticket_bln U7 nach Spandau 3 Kontrolleure in Uniform, alle M.\nHinter der Möckernbrücke"
      },
      "geometry": {
        "coordinates": [
          13.383256,
          52.498944
        ],
        "type": "Point"
      },
      "id": "57c3e72e4b42a2a3755a4dbad056d23d"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Bernauer Str",
          "id": "900000007110"
        },
        "direction": null,
        "line": null,
        "timestamp": "1583246280000",
        "text": "RT @sasoufilou: Kontrolleure Ubhf Bernauer Straße, grad ausgestiegen. @noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.396231,
          52.537994
        ],
        "type": "Point"
      },
      "id": "586fb01238b699be1afbbda7d53a3927"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Friedrichstr",
          "id": "900000100001"
        },
        "direction": null,
        "line": {
          "name": "S7",
          "id": null
        },
        "timestamp": "1478711022000",
        "text": "RT @dumbkaido: @ber_ohne_ticket #Kontrolle S7, gerade Friedrichstraße."
      },
      "geometry": {
        "coordinates": [
          13.387153,
          52.52027
        ],
        "type": "Point"
      },
      "id": "5d1fd89afdaa121666285a862c91c58c"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Alexanderplatz",
          "id": "900000100003"
        },
        "direction": null,
        "line": {
          "name": "U8",
          "id": null
        },
        "timestamp": "1508311785000",
        "text": "RT @dumbkaido: @ber_ohne_ticket #kontrolle U8 Alexanderplatz beim Aussteigen."
      },
      "geometry": {
        "coordinates": [
          13.411267,
          52.521512
        ],
        "type": "Point"
      },
      "id": "5e5f5b07867f00847e98a67e10155f47"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Bellevue",
          "id": "900000003102"
        },
        "direction": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "line": null,
        "timestamp": "1580195636000",
        "text": "RT @tofu_li: Kontrollen Bellevue sbahnen Richtung hbf #noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.347098,
          52.519951
        ],
        "type": "Point"
      },
      "id": "60145ce78636527614d8618ed8a9d825"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1580714331000",
        "text": "RT @LaLunaNeko: Kontrolleure auf Ost West Verbindung am Hauptbahnhof mit schwarzem Basecap und langen dunkelbraunen Bart und schwarzer Jack…"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "629147036ac432f25b36a2b1d06206a3"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Gneisenaustr",
          "id": "900000016101"
        },
        "direction": {
          "name": "Rudow",
          "id": "900000083201"
        },
        "line": {
          "name": "U7",
          "id": null
        },
        "timestamp": "1581505915000",
        "text": "RT @DDiekeinerkennt: @noticket_bln zwei Typen mit schwarzen Klamotten U7 Richtung Rudow . Gneisenau Straße würde ich rausgesunken"
      },
      "geometry": {
        "coordinates": [
          13.395382,
          52.491252
        ],
        "type": "Point"
      },
      "id": "63458e9e4eb68122309ea97ee98c2207"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Bülowstr",
          "id": "900000056104"
        },
        "direction": null,
        "line": null,
        "timestamp": "1580816250000",
        "text": "RT @hackthesociety: U Bülowstr zwei Zivis #noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.362452,
          52.497657
        ],
        "type": "Point"
      },
      "id": "6356fabd0b7eccf55a4a73aeea56f2f8"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Mehringdamm",
          "id": "900000017101"
        },
        "direction": null,
        "line": {
          "name": "U6",
          "id": null
        },
        "timestamp": "1486976541000",
        "text": "RT @NickPower3939: @ber_ohne_ticket Fahrkarten Kontrolle #u6 mehringdamm #u7"
      },
      "geometry": {
        "coordinates": [
          13.388153,
          52.493575
        ],
        "type": "Point"
      },
      "id": "643193dccd9b52b05355f29f5af58b88"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Tempelhof",
          "id": "900000068201"
        },
        "direction": null,
        "line": {
          "name": "S41",
          "id": null
        },
        "timestamp": "1583428742000",
        "text": "RT @sofakante: Kontrolle S41, gerade S Tempelhof #noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.385754,
          52.470694
        ],
        "type": "Point"
      },
      "id": "6546f43980ffc22ebe16f7475aea6a06"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Nordbahnhof",
          "id": "900000007104"
        },
        "direction": null,
        "line": null,
        "timestamp": "1578053989000",
        "text": "RT @BlackCatMuc: 2 Kontrolleure Nordbahnhof am Gleis. Sehen eher aus wie 2 Väter auf dem Weg zum Elternabend @noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.388794,
          52.531673
        ],
        "type": "Point"
      },
      "id": "655dc60a03f91e8522fc9eddd05e6dcb"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Adlershof",
          "id": "900000193002"
        },
        "direction": null,
        "line": null,
        "timestamp": "1459345272000",
        "text": "RT @NickPower3939: @ber_ohne_ticket Kontrolle am Bahnsteig in #Adlershof und in den Bahnen in der Nähe"
      },
      "geometry": {
        "coordinates": [
          13.540553,
          52.435102
        ],
        "type": "Point"
      },
      "id": "66526a28f4317adba4ca3f154a9188da"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Oranienburger Tor",
          "id": "900000100019"
        },
        "direction": null,
        "line": {
          "name": "U6",
          "id": null
        },
        "timestamp": "1580924868000",
        "text": "RT @ideesnoires: @noticket_bln 5 kontris oranienburger tor u6"
      },
      "geometry": {
        "coordinates": [
          13.387587,
          52.525163
        ],
        "type": "Point"
      },
      "id": "67d1c359de0913c6aa3f1519b686c52b"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Karow",
          "id": "900000143001"
        },
        "direction": {
          "name": "Blankenfelde",
          "id": "900000245027"
        },
        "line": {
          "name": "S2",
          "id": null
        },
        "timestamp": "1571223899000",
        "text": "RT @MaiChaot: #Kontrolle #S2 Richtung Blankenfelde Höhe Karow @noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.470081,
          52.615755
        ],
        "type": "Point"
      },
      "id": "67f12c2c89cfaaf96ad443f10a26db41"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Jannowitzbrücke",
          "id": "900000100004"
        },
        "direction": {
          "name": "Westkreuz",
          "id": "900000024102"
        },
        "line": {
          "name": "S5",
          "id": null
        },
        "timestamp": "1582279743000",
        "text": "RT @antifaschwuchtl: Ticketkontrolle S5 nach Westkreuz, Typ blaue Jeans schwarze Jacke. Jannowitzbrücke eingestiegen, Alexanderplatz ausges…"
      },
      "geometry": {
        "coordinates": [
          13.418027,
          52.5155
        ],
        "type": "Point"
      },
      "id": "683da8a4ffd602257f7361e2090019cb"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Zoologischer Garten",
          "id": "900000023201"
        },
        "direction": null,
        "line": {
          "name": "U9",
          "id": null
        },
        "timestamp": "1582874040000",
        "text": "RT @fickDieNSAFD: #noticket_bln\nKontrolle U9 Zoo Richtung Osloer Straße"
      },
      "geometry": {
        "coordinates": [
          13.332707,
          52.506921
        ],
        "type": "Point"
      },
      "id": "69f3b8f9d171c7e54ab4f10238e45ebc"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Moritzplatz",
          "id": "900000013101"
        },
        "direction": null,
        "line": null,
        "timestamp": "1576142274000",
        "text": "RT @rand_gestalt: Am U-Bhf Moritzplatz werden aktuell alle Aus- und Umsteigenden kontrolliert. @noticket_bln #kontrolle"
      },
      "geometry": {
        "coordinates": [
          13.410947,
          52.503739
        ],
        "type": "Point"
      },
      "id": "6d18b2199d6d307901c647a10aa6c731"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Friedrichstr",
          "id": "900000100001"
        },
        "direction": {
          "name": "Spandau",
          "id": "900000029101"
        },
        "line": {
          "name": "S9",
          "id": null
        },
        "timestamp": "1581590346000",
        "text": "RT @antifaschwuchtl: Fahrkartkontrolle S9 nach Spandau Typ mit blauer jeans und schwarzer Jacke, Friedrichstr eingestiegen #noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.387153,
          52.52027
        ],
        "type": "Point"
      },
      "id": "6d25276db9e54ed9be04a15e1e80e622"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Wedding",
          "id": "900000009104"
        },
        "direction": null,
        "line": {
          "name": "U6",
          "id": null
        },
        "timestamp": "1482060384000",
        "text": "RT @dumbkaido: @ber_ohne_ticket #Kontrolle U6, gerade Wedding."
      },
      "geometry": {
        "coordinates": [
          13.36606,
          52.542732
        ],
        "type": "Point"
      },
      "id": "6ead35da1754de41adf578eeedeeb72d"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Griebnitzsee",
          "id": "900000230003"
        },
        "direction": null,
        "line": {
          "name": "S7",
          "id": null
        },
        "timestamp": "1574680992000",
        "text": "RT @besorgterburger: 1220 Uhr\nS7 gerade zwischen Griebnitzsee &amp; Wannsee. Wahrscheinlich auch wieder umkehrt.\nP1 rote Haare Undercut und Bri…"
      },
      "geometry": {
        "coordinates": [
          13.128917,
          52.393988
        ],
        "type": "Point"
      },
      "id": "7072154d4d94fb37d0e06f8a6a2050e8"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Rohrdamm",
          "id": "900000036101"
        },
        "direction": {
          "name": "Rudow",
          "id": "900000083201"
        },
        "line": {
          "name": "U7",
          "id": null
        },
        "timestamp": "1493371716000",
        "text": "RT @frttnfieber: Drei Kontrolleur_innen (Alle BVG Uniform) in der U7 Richtung Rudow, Station Rohrdamm @ber_ohne_ticket"
      },
      "geometry": {
        "coordinates": [
          13.26287,
          52.536904
        ],
        "type": "Point"
      },
      "id": "70e8dacdceacdbe45c9706f2989fae1c"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Französische Str",
          "id": "900000100027"
        },
        "direction": {
          "name": "Mehringdamm",
          "id": "900000017101"
        },
        "line": {
          "name": "U6",
          "id": null
        },
        "timestamp": "1580814417000",
        "text": "RT @Frau_Koenig: 12:03 u6 Französische Straße, Richtung Mehringdamm. 2 Zivis, männlich gelesen. Beide komplett in schwarz. 1 mit Air-Jordan…"
      },
      "geometry": {
        "coordinates": [
          13.390088,
          52.514771
        ],
        "type": "Point"
      },
      "id": "71b275e8e449077bb80b3003469539d5"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Wittenbergplatz",
          "id": "900000056101"
        },
        "direction": null,
        "line": null,
        "timestamp": "1501856551000",
        "text": "RT @NickPower3939: @ber_ohne_ticket Fahrkarte Kontrolle #Wittenbergplatz."
      },
      "geometry": {
        "coordinates": [
          13.342561,
          52.502109
        ],
        "type": "Point"
      },
      "id": "72cb0483b0c4c2b68b99235f2d3561c7"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Friedenau",
          "id": "900000060101"
        },
        "direction": null,
        "line": {
          "name": "S1",
          "id": null
        },
        "timestamp": "1575666404000",
        "text": "RT @sim030: #S1 \"kein Halt in Friedenau, Grund: fehlende Bahnhofsbeleuchtung\" @noticket_bln @SBahnBerlin"
      },
      "geometry": {
        "coordinates": [
          13.340598,
          52.470002
        ],
        "type": "Point"
      },
      "id": "766d8b2f097240aa03e353599ab1fbdb"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Westend",
          "id": "900000026207"
        },
        "direction": null,
        "line": {
          "name": "S46",
          "id": null
        },
        "timestamp": "1583992871000",
        "text": "RT @lysalia_: @noticket_bln 06:59 S46 r. Westend ab hermannstrasse\n2 Personen einer hat ne halbglatze und ne schwarze Jacke an.\nSteigen ans…"
      },
      "geometry": {
        "coordinates": [
          13.284237,
          52.51861
        ],
        "type": "Point"
      },
      "id": "76e7a94f32203bf86e499d2b6d7988d0"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Schönholz",
          "id": "900000085201"
        },
        "direction": null,
        "line": {
          "name": "S2",
          "id": null
        },
        "timestamp": "1521819473000",
        "text": "RT @NickPower3939: Schönholz. #s26 Ticketkontrolle!\n@ber_ohne_ticket"
      },
      "geometry": {
        "coordinates": [
          13.380242,
          52.571901
        ],
        "type": "Point"
      },
      "id": "77e635cc49844d1e56b71892603ac325"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Nollendorfplatz",
          "id": "900000056102"
        },
        "direction": null,
        "line": null,
        "timestamp": "1499865910000",
        "text": "RT @NickPower3939: @ber_ohne_ticket Kontrolleure schwieren am Nollendorfplatz Rum..."
      },
      "geometry": {
        "coordinates": [
          13.353825,
          52.499644
        ],
        "type": "Point"
      },
      "id": "7828e365000e4b63d14a3c008766e3eb"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Prenzlauer Allee",
          "id": "900000110002"
        },
        "direction": null,
        "line": null,
        "timestamp": "1580804383000",
        "text": "RT @pascale1312: #noticket_bln\nPrenzlauer Allee Richtung Osten Zwei kontrolettis  P1 schwarze gegelte Haare schick angezogen mit kurzmantel…"
      },
      "geometry": {
        "coordinates": [
          13.427419,
          52.544802
        ],
        "type": "Point"
      },
      "id": "79c7d9f5500942fcd2117532722ba882"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Möckernbrücke",
          "id": "900000017104"
        },
        "direction": null,
        "line": {
          "name": "U7",
          "id": null
        },
        "timestamp": "1588939318416",
        "text": "RT @LegalizeYou: @noticket_bln #noticket_bln\nKontrollen an der U7 Möckernbrücke\n\n3-4 Dudes, alle in blauen Westen"
      },
      "geometry": {
        "coordinates": [
          13.383256,
          52.498944
        ],
        "type": "Point"
      },
      "id": "79efad7c42143cc2b66a5bd4cfef46e3"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Savignyplatz",
          "id": "900000024203"
        },
        "direction": null,
        "line": {
          "name": "S7",
          "id": null
        },
        "timestamp": "1576759275000",
        "text": "RT @besorgterburger: #Kontrolle 1337 Uhr.\nS7 gerade Savignyplatz \nP1 Schwarze Jacke blaue Umhängetasche blaue jeans kurze Haare. Steigt jet…"
      },
      "geometry": {
        "coordinates": [
          13.319003,
          52.505223
        ],
        "type": "Point"
      },
      "id": "7a9f308a83a6cac845a1976f34894f86"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Rathaus Spandau",
          "id": "900000029302"
        },
        "direction": {
          "name": "Rudow",
          "id": "900000083201"
        },
        "line": {
          "name": "U7",
          "id": null
        },
        "timestamp": "1499153577000",
        "text": "RT @frttnfieber: Drei Kontrolleure in Uniform U7 Richtung Rudow, eingestiegen Rathaus Spandau, ausgestiegen Altstadt Spandau @ber_ohne_tick…"
      },
      "geometry": {
        "coordinates": [
          13.199891,
          52.535798
        ],
        "type": "Point"
      },
      "id": "7e87b8a66cd3f28a427ebd6f6228a042"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Pankstr",
          "id": "900000009203"
        },
        "direction": {
          "name": "Wittenau",
          "id": "900000096101"
        },
        "line": {
          "name": "U8",
          "id": null
        },
        "timestamp": "1582047176000",
        "text": "RT @LegalizeYou: @noticket_bln   #noticket_bln\n\nGrad u8 richtung wittenau höhe pankstr\n\nDünner kleiner dude mit bomberjacke"
      },
      "geometry": {
        "coordinates": [
          13.381837,
          52.552255
        ],
        "type": "Point"
      },
      "id": "7f3df53a4ac015a50427ee2634c72d7c"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Friedrichstr",
          "id": "900000100001"
        },
        "direction": null,
        "line": {
          "name": "S7",
          "id": null
        },
        "timestamp": "1478105920000",
        "text": "RT @dumbkaido: @ber_ohne_ticket S7, gerade bei Friedrichstraße ausgestiegen. #kontrolle"
      },
      "geometry": {
        "coordinates": [
          13.387153,
          52.52027
        ],
        "type": "Point"
      },
      "id": "7fcd33b75a463cacf8d5a1c12235f2c6"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Brandenburger Tor",
          "id": "900000100025"
        },
        "direction": null,
        "line": {
          "name": "S1",
          "id": null
        },
        "timestamp": "1572179961000",
        "text": "RT @sim030: #Kontros auf Tourijagd. #S1 zwischen Potsdamer und Brandenburger Tor. @noticket_bln\n\nNummer des #Kontro war: 79564"
      },
      "geometry": {
        "coordinates": [
          13.381937,
          52.516511
        ],
        "type": "Point"
      },
      "id": "824eebdbcc57c35e7f903ba6fe181785"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Mexikoplatz",
          "id": "900000050301"
        },
        "direction": null,
        "line": {
          "name": "S1",
          "id": null
        },
        "timestamp": "1511199882000",
        "text": "RT @grauersweater: @ber_ohne_ticket Kontrolle S1 - Mexikoplatz, eine frau und ein mann, beide allblack angezogen"
      },
      "geometry": {
        "coordinates": [
          13.23206,
          52.437166
        ],
        "type": "Point"
      },
      "id": "82ae5e4d200430b80dc09d401b860bb2"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Görlitzer Bahnhof",
          "id": "900000014101"
        },
        "direction": null,
        "line": {
          "name": "U1",
          "id": null
        },
        "timestamp": "1575476217000",
        "text": "RT @_die_katrin: 1715. auf der u1 kontrollettis am görlitzer bhf. zivil. beide schwarze jacke, schwarze mütze/basecap. @noticket_bln #ticke…"
      },
      "geometry": {
        "coordinates": [
          13.428468,
          52.499035
        ],
        "type": "Point"
      },
      "id": "82d4eedb8b3e75e5e021655fc6171d49"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Seestr",
          "id": "900000009103"
        },
        "direction": null,
        "line": null,
        "timestamp": "1501743864000",
        "text": "RT @dumbkaido: @ber_ohne_ticket #kontrolle Horde uniformierter Kontrollettis in der Tram Richtung Virchow Klinikum, gleich Seestraße."
      },
      "geometry": {
        "coordinates": [
          13.351969,
          52.55047
        ],
        "type": "Point"
      },
      "id": "82f64f5d700f2d44b587fac61c83c530"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Griebnitzsee",
          "id": "900000230003"
        },
        "direction": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "line": {
          "name": "S1",
          "id": null
        },
        "timestamp": "1531226126000",
        "text": "RT @grauersweater: @ber_ohne_ticket #Kontrolle S1 Griebnitzsee nach Potsdam Hbf - Zwei typen mit Cappys"
      },
      "geometry": {
        "coordinates": [
          13.128917,
          52.393988
        ],
        "type": "Point"
      },
      "id": "84b8503399d599eac328f2225517ce88"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Neukölln",
          "id": "900000078201"
        },
        "direction": null,
        "line": {
          "name": "S41",
          "id": null
        },
        "timestamp": "1456729861000",
        "text": "RT @NickPower3939: @ber_ohne_ticket Kontrolle Ringbahn zwischen Tempelhof und Neukölln #s42 #s46 s41 #s47"
      },
      "geometry": {
        "coordinates": [
          13.441986,
          52.469385
        ],
        "type": "Point"
      },
      "id": "84e1a5860877454cfc4eda5b6648d7e8"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Kottbusser Tor",
          "id": "900000013102"
        },
        "direction": null,
        "line": {
          "name": "U8",
          "id": null
        },
        "timestamp": "1575213522000",
        "text": "RT @_die_katrin: 1615. kontrolle in der u8, rund um kotti. in zivil in der bahn und bvg/polizei aufm bahnsteig. /@noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.417748,
          52.499047
        ],
        "type": "Point"
      },
      "id": "85d1d50f60bf185d5a39acc9279483a3"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Weinmeisterstr",
          "id": "900000100051"
        },
        "direction": null,
        "line": {
          "name": "U8",
          "id": null
        },
        "timestamp": "1576933402000",
        "text": "RT @stadtprolet: @noticket_bln Frau lange braune Haare + hipster Jacke, Mann sehr groß + kurze schwarze Haare, U8 U Bhf Weinmeisterstraße #…"
      },
      "geometry": {
        "coordinates": [
          13.405305,
          52.525376
        ],
        "type": "Point"
      },
      "id": "86e629ecdf5d102c9b57cf3fe0879484"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Görlitzer Bahnhof",
          "id": "900000014101"
        },
        "direction": null,
        "line": {
          "name": "U1",
          "id": null
        },
        "timestamp": "1486137045000",
        "text": "RT @dumbkaido: @ber_ohne_ticket #kontrolle U1, gerade Görlitzer Bahnhof ausgestiegen, vermutlich 2 Männer*"
      },
      "geometry": {
        "coordinates": [
          13.428468,
          52.499035
        ],
        "type": "Point"
      },
      "id": "897449ebbec423e2d250fa41ad233627"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Wannsee",
          "id": "900000053301"
        },
        "direction": null,
        "line": {
          "name": "S7",
          "id": null
        },
        "timestamp": "1575290662000",
        "text": "RT @karloh_oh: @noticket_bln s7 wannsee-&gt; Griebnitzsee"
      },
      "geometry": {
        "coordinates": [
          13.179099,
          52.421457
        ],
        "type": "Point"
      },
      "id": "89dade9dbbc0cb596f576963d4533aa5"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Hermannplatz",
          "id": "900000078101"
        },
        "direction": {
          "name": "Rudow",
          "id": "900000083201"
        },
        "line": {
          "name": "U7",
          "id": null
        },
        "timestamp": "1579077087000",
        "text": "RT @polyamIzzy: Riesen Aufgebot mit Kontrolle und Polizei an der U7 am Hermannplatz, Ausstiegskontrolle (!) am Bahnsteig Richtung Rudow \n\n#…"
      },
      "geometry": {
        "coordinates": [
          13.42472,
          52.486957
        ],
        "type": "Point"
      },
      "id": "8b62ec524bcb56e179c477a0f705e83d"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Wannsee",
          "id": "900000053301"
        },
        "direction": {
          "name": "Nikolassee",
          "id": "900000052201"
        },
        "line": {
          "name": "S1",
          "id": null
        },
        "timestamp": "1581408325000",
        "text": "RT @karloh_oh: @noticket_bln s1 Kontrolle Wannsee Richtung nikolassee"
      },
      "geometry": {
        "coordinates": [
          13.179099,
          52.421457
        ],
        "type": "Point"
      },
      "id": "8ed023b27eaa426cf22073ff62756060"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Schöneweide",
          "id": "900000192001"
        },
        "direction": {
          "name": "Westend",
          "id": "900000026207"
        },
        "line": {
          "name": "S46",
          "id": null
        },
        "timestamp": "1574842129000",
        "text": "RT @lysalia_: @noticket_bln #noticketbln 09:06\nS46 Richtung Westend\nSind gerade s schöneweide eingestiegen\n\nEiner trägt ne cap in Deutschla…"
      },
      "geometry": {
        "coordinates": [
          13.510149,
          52.454611
        ],
        "type": "Point"
      },
      "id": "90346787a604923380ada6d2679f22dd"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Wedding",
          "id": "900000009104"
        },
        "direction": null,
        "line": null,
        "timestamp": "1581583195000",
        "text": "RT @Schlafigel: Kontrolle s Wedding Richtung Moabit. 2 Typen, einer mit gelber Jacke, andere etwas älter leicht untersetzt und mit Brille…"
      },
      "geometry": {
        "coordinates": [
          13.36606,
          52.542732
        ],
        "type": "Point"
      },
      "id": "90d156a716d86673aa298a2720e3e27c"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Hermannstr",
          "id": "900000079221"
        },
        "direction": null,
        "line": {
          "name": "S42",
          "id": null
        },
        "timestamp": "1502814717000",
        "text": "RT @NickPower3939: Fahrkarte Kontrolle Ringbahn #s42 Hermannstraße @ber_ohne_ticket"
      },
      "geometry": {
        "coordinates": [
          13.431704,
          52.467181
        ],
        "type": "Point"
      },
      "id": "917b4f2357e28db979a8c523007602b3"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Yorckstr",
          "id": "900000057103"
        },
        "direction": {
          "name": "Rudow",
          "id": "900000083201"
        },
        "line": {
          "name": "U7",
          "id": null
        },
        "timestamp": "1579276822000",
        "text": "RT @joska_fischer: #noticket_bln u7 von yorckstrasse richtung rudow. Kleistpark ausgestiegen"
      },
      "geometry": {
        "coordinates": [
          13.370429,
          52.492766
        ],
        "type": "Point"
      },
      "id": "92009ab1e9366dc51ab24fea9d4fcf51"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Stadtmitte",
          "id": "900000100011"
        },
        "direction": {
          "name": "Friedrichstr",
          "id": "900000100001"
        },
        "line": {
          "name": "U6",
          "id": null
        },
        "timestamp": "1572773682000",
        "text": "RT @Frau_Koenig: #noticket_bln 10.33: u6 Richtung Friedrichstraße. Gerade Stadtmitte. \n2 Kontrolleure, beide schwarze Daunenjacken."
      },
      "geometry": {
        "coordinates": [
          13.389719,
          52.511495
        ],
        "type": "Point"
      },
      "id": "93dbc46d00e8b10179824febb2043239"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Charlottenburg",
          "id": "900000024101"
        },
        "direction": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "line": {
          "name": "S7",
          "id": null
        },
        "timestamp": "1580121912000",
        "text": "RT @aluhutt: Uniformierte #Kontrolle⁠tis in der S7 Richtung Potsdam Hbf; gerade an der S Charlottenburg. #noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.305212,
          52.505052
        ],
        "type": "Point"
      },
      "id": "9469c37b9607e35980b24e003e324a95"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Tempelhof",
          "id": "900000068201"
        },
        "direction": null,
        "line": {
          "name": "U6",
          "id": null
        },
        "timestamp": "1478769008000",
        "text": "RT @NickPower3939: @ber_ohne_ticket Fahrkartenkontrolle #u6 Tempelhof"
      },
      "geometry": {
        "coordinates": [
          13.385754,
          52.470694
        ],
        "type": "Point"
      },
      "id": "94fbaefff6053edb1b5143606761a824"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Greifswalder Str",
          "id": "900000110003"
        },
        "direction": null,
        "line": null,
        "timestamp": "1584002615000",
        "text": "RT @Schlafigel: Kontrolle Greifswalder str. Eine Frau, kurze Haare Bomberjacke #noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.438356,
          52.540724
        ],
        "type": "Point"
      },
      "id": "982603eb33775588a692f2de7837727a"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Naturkundemuseum",
          "id": "900000100009"
        },
        "direction": null,
        "line": {
          "name": "U6",
          "id": null
        },
        "timestamp": "1589118805429",
        "text": "RT @derkaktusgarten: #noticket_bln U6 Nauturkundemuseum Kontrolleur mit weißem Hemd"
      },
      "geometry": {
        "coordinates": [
          13.382415,
          52.531254
        ],
        "type": "Point"
      },
      "id": "9898301a9d5b93eb3f2272858014c45b"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Schlesisches Tor",
          "id": "900000014102"
        },
        "direction": {
          "name": "Krumme Lanke",
          "id": "900000050201"
        },
        "line": {
          "name": "U1",
          "id": null
        },
        "timestamp": "1583398955000",
        "text": "RT @frl_rastlos: #noticket_bln U1 Richtung Krumme Lanke, nächster Halt Schlesisches Tor - Kontrolle!"
      },
      "geometry": {
        "coordinates": [
          13.441791,
          52.501147
        ],
        "type": "Point"
      },
      "id": "98eee876d1e2ca61826ad1f2402ba8b6"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Südkreuz",
          "id": "900000058101"
        },
        "direction": null,
        "line": null,
        "timestamp": "1577101839000",
        "text": "RT @IsaacOnBike: Südkreuz jetzt ausgestiegen. 2 Männer dunkle Jacken. Einer Glatze der andere blaue Umhänhmgetasche. #noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.365579,
          52.475468
        ],
        "type": "Point"
      },
      "id": "99232da7718329a30d6be9282ed2d446"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Bellevue",
          "id": "900000003102"
        },
        "direction": null,
        "line": null,
        "timestamp": "1576175236000",
        "text": "RT @exiton: S Bellevue ausgestiegen #noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.347098,
          52.519951
        ],
        "type": "Point"
      },
      "id": "9984d65a211e76b93fef328c1954f1da"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Möckernbrücke",
          "id": "900000017104"
        },
        "direction": {
          "name": "Rudow",
          "id": "900000083201"
        },
        "line": {
          "name": "U7",
          "id": null
        },
        "timestamp": "1580404483000",
        "text": "RT @besorgterburger: KonTrolle:\nU7 jetzt gerade noch Richtung Rudow, Höhe Möckernbrücke. P1 Jacke mit Fell an Kapuze und Cap. P2 jeans und…"
      },
      "geometry": {
        "coordinates": [
          13.383256,
          52.498944
        ],
        "type": "Point"
      },
      "id": "9ad038750069db0d2aae73d3688659a5"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1580887876000",
        "text": "RT @tofu_li: Kontrollen hbf Richtung Friedrichdtraße in der sbahn. cap und Brille.\n#noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "9d6344eb5f5ed5a1a52445719266e97b"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Möckernbrücke",
          "id": "900000017104"
        },
        "direction": null,
        "line": {
          "name": "U7",
          "id": null
        },
        "timestamp": "1588939623730",
        "text": "RT @LegalizeYou: @noticket_bln #noticket_bln\nKontrollen an der U7 Möckernbrücke\n\n3-4 Dudes, alle in blauen Westen"
      },
      "geometry": {
        "coordinates": [
          13.383256,
          52.498944
        ],
        "type": "Point"
      },
      "id": "9e32f5bb8a1ecfe19c017fb7c2a97bbf"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Bellevue",
          "id": "900000003102"
        },
        "direction": null,
        "line": {
          "name": "M1",
          "id": null
        },
        "timestamp": "1573573473000",
        "text": "RT @Kleinstadtknig1: @noticket_bln \nZwei Männer, Station Bellevue m1\nWeisser Rollkragen  rote käppi.\nM2 brille blonde Haare nach hinten geg…"
      },
      "geometry": {
        "coordinates": [
          13.347098,
          52.519951
        ],
        "type": "Point"
      },
      "id": "a075f3af5609675939e027f60c602e7b"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Wannsee",
          "id": "900000053301"
        },
        "direction": null,
        "line": {
          "name": "S7",
          "id": null
        },
        "timestamp": "1583930004000",
        "text": "RT @karloh_oh: @noticket_bln Kontrolle s7 Wannsee -&gt; Griebnitzsee"
      },
      "geometry": {
        "coordinates": [
          13.179099,
          52.421457
        ],
        "type": "Point"
      },
      "id": "a09be1bfd7b842bd1e1a1a3b2191b6ce"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berliner Str",
          "id": "900000044201"
        },
        "direction": null,
        "line": {
          "name": "U9",
          "id": null
        },
        "timestamp": "1584083380000",
        "text": "RT @fickDieNSAFD: #noticket_bln\nU9 Berliner Straße in Richtung Osloer"
      },
      "geometry": {
        "coordinates": [
          13.331355,
          52.487047
        ],
        "type": "Point"
      },
      "id": "a2422f1bc905747b97912ca96c2c6953"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Storkower Str",
          "id": "900000110012"
        },
        "direction": null,
        "line": null,
        "timestamp": "1581122608000",
        "text": "RT @ngeisemeyer: Kontrolle auf dem Ring Richtung Uhrzeigersinn zwischen Storkower Straße und Landsberger Allee #noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.464884,
          52.524195
        ],
        "type": "Point"
      },
      "id": "a2d910e1830458b33d47b4edfee9ee65"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Friedrichstr",
          "id": "900000100001"
        },
        "direction": null,
        "line": {
          "name": "U6",
          "id": null
        },
        "timestamp": "1501857883000",
        "text": "RT @weakaside_: @ber_ohne_ticket u6 friedrichstraße zwei stämmige dunkelhaarige männer, dunkle tshirts einer mit Bart und Mütze #kontrolle"
      },
      "geometry": {
        "coordinates": [
          13.387153,
          52.52027
        ],
        "type": "Point"
      },
      "id": "a3c7a837d09dac3092b0d51ae36874e1"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Gleisdreieck",
          "id": "900000017103"
        },
        "direction": null,
        "line": {
          "name": "U2",
          "id": null
        },
        "timestamp": "1454515001000",
        "text": "RT @NickPower3939: @ber_ohne_ticket #ticketkontrolle u2 Richtung Warschauer steigen Gleisdreieck gerade ein"
      },
      "geometry": {
        "coordinates": [
          13.374293,
          52.499587
        ],
        "type": "Point"
      },
      "id": "a4b3dba7f634fa3550500fbae2d5ff16"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Stadtmitte",
          "id": "900000100011"
        },
        "direction": {
          "name": "Alt-Tempelhof",
          "id": "900000068202"
        },
        "line": {
          "name": "U6",
          "id": null
        },
        "timestamp": "1579805525000",
        "text": "RT @aluhutt: Fahrscheinkontrolle in der U6 (Stadtmitte) in der Bahn Richtung Alt-Tempelhof. Ein Mann mit Bart, schwarze Haare, schwarze Jac…"
      },
      "geometry": {
        "coordinates": [
          13.389719,
          52.511495
        ],
        "type": "Point"
      },
      "id": "a709c30958dc3393fb101495b09f1fbd"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Alexanderplatz",
          "id": "900000100003"
        },
        "direction": null,
        "line": {
          "name": "U5",
          "id": null
        },
        "timestamp": "1509987741000",
        "text": "RT @dumbkaido: @ber_ohne_ticket #kontrolle U5, gleich Alexanderplatz."
      },
      "geometry": {
        "coordinates": [
          13.411267,
          52.521512
        ],
        "type": "Point"
      },
      "id": "a78b7f72efa31a8052d7a407111ea5e1"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Hackescher Markt",
          "id": "900000100002"
        },
        "direction": {
          "name": "Flughafen Berlin-Schönefeld",
          "id": "900000260005"
        },
        "line": {
          "name": "S9",
          "id": null
        },
        "timestamp": "1581761422000",
        "text": "RT @aluhutt: #Fahrscheinkontrolle in der S9 Richtung Schönefeld auf Höhe S Hackescher Markt. Ein Mann und eine Frau in schwarz. #noticket_b…"
      },
      "geometry": {
        "coordinates": [
          13.402358,
          52.522607
        ],
        "type": "Point"
      },
      "id": "a970f44d342ee4417eefea5920bd94fd"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Mexikoplatz",
          "id": "900000050301"
        },
        "direction": {
          "name": "Frohnau",
          "id": "900000092201"
        },
        "line": {
          "name": "S1",
          "id": null
        },
        "timestamp": "1482394242000",
        "text": "RT @frttnfieber: @ber_ohne_ticket Mexikoplatz, S1 nach Frohnau, zwei Männer, einer Cappy und Bomberjacke, ziemlich groß"
      },
      "geometry": {
        "coordinates": [
          13.23206,
          52.437166
        ],
        "type": "Point"
      },
      "id": "ab3a6c6c8a438a56295b6afc11df13d4"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Kottbusser Tor",
          "id": "900000013102"
        },
        "direction": null,
        "line": {
          "name": "U1",
          "id": null
        },
        "timestamp": "1518008567000",
        "text": "RT @dumbkaido: @ber_ohne_ticket #kontrolle U1, gerade Kottbusser Tor ausgestiegen."
      },
      "geometry": {
        "coordinates": [
          13.417748,
          52.499047
        ],
        "type": "Point"
      },
      "id": "b00b786bdd2ae9ff50913d344e7b1f6f"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Warschauer Str",
          "id": "900000120004"
        },
        "direction": {
          "name": "Ostkreuz",
          "id": "900000120003"
        },
        "line": null,
        "timestamp": "1584100538000",
        "text": "RT @Frau_Koenig: 1253 S-Bahn Warschauer Str., Richtung Ostkreuz: min. 3 Kontrollierende (männlich gelesen), alle schwarze Jacken.\n#noticket…"
      },
      "geometry": {
        "coordinates": [
          13.448721,
          52.505889
        ],
        "type": "Point"
      },
      "id": "b5649c3cc91a2f5c34b785de839d54f5"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Schöneweide",
          "id": "900000192001"
        },
        "direction": {
          "name": "Westend",
          "id": "900000026207"
        },
        "line": {
          "name": "S46",
          "id": null
        },
        "timestamp": "1572417994000",
        "text": "RT @ArthurPobel: #noticket_bln s46 nach Westend, auf höhe Schöneweide, zwei Kontrolleure, einmal m, einmal w"
      },
      "geometry": {
        "coordinates": [
          13.510149,
          52.454611
        ],
        "type": "Point"
      },
      "id": "b6575dcc608c8a15d925ae12a6a2bb1d"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Wildau",
          "id": "900000260002"
        },
        "direction": {
          "name": "Westend",
          "id": "900000026207"
        },
        "line": {
          "name": "S46",
          "id": null
        },
        "timestamp": "1575558789000",
        "text": "RT @lysalia_: 16:10\nS46 r. KW\nAb Zeuthen\nSteigen in wildau wieder aus um dann die S46 nach Westend zu nehmen\nP1 hat ne Mütze mit Sternen au…"
      },
      "geometry": {
        "coordinates": [
          13.633587,
          52.319205
        ],
        "type": "Point"
      },
      "id": "b96677c7041fa7d983eeefbf449ba871"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Kurfürstenstr",
          "id": "900000005201"
        },
        "direction": null,
        "line": {
          "name": "U1",
          "id": null
        },
        "timestamp": "1504704822000",
        "text": "RT @alylives: @ber_ohne_ticket U1 Höhe Kurfürstenstraße zwei Männer, einer mit Bart, einer mit grauem cap #ticketkontrolle"
      },
      "geometry": {
        "coordinates": [
          13.362814,
          52.49981
        ],
        "type": "Point"
      },
      "id": "ba30fb9132f2332eec8888a4938cd21a"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Naturkundemuseum",
          "id": "900000100009"
        },
        "direction": null,
        "line": {
          "name": "U6",
          "id": null
        },
        "timestamp": "1589118799936",
        "text": "#noticket_bln U6 Nauturkundemuseum Kontrolleur mit weißem Hemd"
      },
      "geometry": {
        "coordinates": [
          13.382415,
          52.531254
        ],
        "type": "Point"
      },
      "id": "bea4c21a15d115b995a7961a11e8b031"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Tempelhof",
          "id": "900000068201"
        },
        "direction": null,
        "line": {
          "name": "S41",
          "id": null
        },
        "timestamp": "1508526702000",
        "text": "RT @NickPower3939: @ber_ohne_ticket Fahrkartenkontrolle #ringbahn #s42 #s41 Tempelhof &lt;-&gt; Hermannstraße"
      },
      "geometry": {
        "coordinates": [
          13.385754,
          52.470694
        ],
        "type": "Point"
      },
      "id": "bfb580f71ad942fc51c086618ef50924"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Schlesisches Tor",
          "id": "900000014102"
        },
        "direction": {
          "name": "Uhlandstr",
          "id": "900000023301"
        },
        "line": {
          "name": "U1",
          "id": null
        },
        "timestamp": "1478624037000",
        "text": "RT @dumbkaido: @ber_ohne_ticket U1 Richtung Uhlandstraße Höhe Schlesisches Tor."
      },
      "geometry": {
        "coordinates": [
          13.441791,
          52.501147
        ],
        "type": "Point"
      },
      "id": "c2edecb73fb32583d878947903caee07"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Rosenthaler Platz",
          "id": "900000100023"
        },
        "direction": null,
        "line": {
          "name": "U8",
          "id": null
        },
        "timestamp": "1581092792000",
        "text": "RT @easytarget2000: @noticket_bln Kontrolle auf der U8 auf Höhe Rosenthaler Platz. Zwei große Typen in dunklen Jacken, einer mit dunklem Ca…"
      },
      "geometry": {
        "coordinates": [
          13.401393,
          52.529781
        ],
        "type": "Point"
      },
      "id": "c39b871e22b1d6ae87b582c6cf551de7"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Neukölln",
          "id": "900000078201"
        },
        "direction": null,
        "line": {
          "name": "U7",
          "id": null
        },
        "timestamp": "1581316259000",
        "text": "RT @sasoufilou: U7 Neukölln gerade zwei Kontrolleure ausgestiegen und am Bahnsteig. Zwei Männer, dunkle Kleidung, mind. einer Bart. @notick…"
      },
      "geometry": {
        "coordinates": [
          13.441986,
          52.469385
        ],
        "type": "Point"
      },
      "id": "c4e794ead8840e52297a8c7c5f42d06b"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Weberwiese",
          "id": "900000120025"
        },
        "direction": null,
        "line": {
          "name": "U5",
          "id": null
        },
        "timestamp": "1581599895000",
        "text": "RT @schlechteIdee: #noticket_bln u5 Höhe Weberwiese zwei mal in zwei Stunden kontrolliert"
      },
      "geometry": {
        "coordinates": [
          13.443278,
          52.516848
        ],
        "type": "Point"
      },
      "id": "c845e24f892f00471620abfb7dcfcbb6"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "direction": {
          "name": "Zoologischer Garten",
          "id": "900000023201"
        },
        "line": null,
        "timestamp": "1581011206000",
        "text": "RT @stadtprolet: @noticket_bln Zwei Kontrolleure S Bhf Hbf Richtung Zoo, männlich, jung, schwarze gegelte Haare #noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.368924,
          52.525847
        ],
        "type": "Point"
      },
      "id": "c99bb24d02beca88a9bf10eea559759c"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Oranienburger Tor",
          "id": "900000100019"
        },
        "direction": null,
        "line": {
          "name": "U6",
          "id": null
        },
        "timestamp": "1477379891000",
        "text": "RT @dumbkaido: @ber_ohne_ticket U6 gerade bei Oranienburger Tor. #Kontrolle"
      },
      "geometry": {
        "coordinates": [
          13.387587,
          52.525163
        ],
        "type": "Point"
      },
      "id": "c9f3658ab680e673b4248a173122d411"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Friedrich-Wilhelm-Platz",
          "id": "900000061102"
        },
        "direction": {
          "name": "Rathaus Steglitz",
          "id": "900000062202"
        },
        "line": {
          "name": "U9",
          "id": null
        },
        "timestamp": "1494608443000",
        "text": "RT @frttnfieber: .@ber_ohne_ticket Kontrolleur_in in Zivil in U9 Richtung Rathaus Steglitz, Höhe Friedrich-Wilhelm-Platz. Lange braune Haar…"
      },
      "geometry": {
        "coordinates": [
          13.328676,
          52.471439
        ],
        "type": "Point"
      },
      "id": "ca1a0563934cd89c65e39bd61623d816"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Potsdamer Platz",
          "id": "900000100020"
        },
        "direction": null,
        "line": null,
        "timestamp": "1582815041000",
        "text": "RT @_noujoum: Kontrolle S Potsdamer Platz, S-Bahn, Typ mit olivgrünem Parka. #noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.376454,
          52.50934
        ],
        "type": "Point"
      },
      "id": "cab1b60b18c4cf6c6d36236c3ebcb77d"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Feuerbachstr",
          "id": "900000063101"
        },
        "direction": {
          "name": "Wannsee",
          "id": "900000053301"
        },
        "line": {
          "name": "S1",
          "id": null
        },
        "timestamp": "1580894587000",
        "text": "RT @ideesnoires: @noticket_bln 2 kontris s1 richtung wannsee station feuerbachstr"
      },
      "geometry": {
        "coordinates": [
          13.332412,
          52.463578
        ],
        "type": "Point"
      },
      "id": "cb44faa5568a094f01fd91a37fca7ca0"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Jannowitzbrücke",
          "id": "900000100004"
        },
        "direction": null,
        "line": {
          "name": "S7",
          "id": null
        },
        "timestamp": "1580114406000",
        "text": "RT @saytine: @noticket_bln #noticket_bln jetzt jannowitzbrücke, in der s7 ri ahrensfelde"
      },
      "geometry": {
        "coordinates": [
          13.418027,
          52.5155
        ],
        "type": "Point"
      },
      "id": "cbaeaec813dda0417d8eae1ffef6a20a"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Südkreuz",
          "id": "900000058101"
        },
        "direction": null,
        "line": {
          "name": "S2",
          "id": null
        },
        "timestamp": "1580458961000",
        "text": "RT @LaLunaNeko: Fahrkartenkontrolleur auf der Linie s25/26 zwischen Teltowstadt und Südkreuz ca. 1,70 groß dicke schwarze Jacke mit Pelz Ka…"
      },
      "geometry": {
        "coordinates": [
          13.365579,
          52.475468
        ],
        "type": "Point"
      },
      "id": "cec3e63cc23a3260e9cb750610124165"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Treptower Park",
          "id": "900000190001"
        },
        "direction": null,
        "line": null,
        "timestamp": "1581507831000",
        "text": "RT @naziwatch_brb: Zwei Kontrolleure Höhe Treptower Park (ein Mann mit blauem Pulli, schwarzer Jacke und schwarzem Cap und eine Frau mit ge…"
      },
      "geometry": {
        "coordinates": [
          13.460887,
          52.493022
        ],
        "type": "Point"
      },
      "id": "cf586745f91347a35564016242931416"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Brandenburger Tor",
          "id": "900000100025"
        },
        "direction": null,
        "line": {
          "name": "S2",
          "id": null
        },
        "timestamp": "1582879797000",
        "text": "RT @_noujoum: Kontrolle S2 Brandenburger Tor, Typ mit schwarzer Jacke und schwarzem Käppi #noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.381937,
          52.516511
        ],
        "type": "Point"
      },
      "id": "cf5fe7d2c2e9192c21f8d247db78ff76"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Prenzlauer Allee",
          "id": "900000110002"
        },
        "direction": null,
        "line": {
          "name": "M6",
          "id": null
        },
        "timestamp": "1570781358000",
        "text": "RT @faktotum691: Kontrolle Schönhauser Prenzlauer Allee Ringbahn, S8 , S85 , zwei Typen, einer mit Käppi, einer braune Jacke, dicker #notic…"
      },
      "geometry": {
        "coordinates": [
          13.427419,
          52.544802
        ],
        "type": "Point"
      },
      "id": "d1af2c572be1a316f834b890107474a7"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Görlitzer Bahnhof",
          "id": "900000014101"
        },
        "direction": null,
        "line": {
          "name": "U1",
          "id": null
        },
        "timestamp": "1571660778000",
        "text": "RT @Jaysmithjs: #noticket_bln u1 görli ri warschauer 2 kontrollettis steigen hinten ein"
      },
      "geometry": {
        "coordinates": [
          13.428468,
          52.499035
        ],
        "type": "Point"
      },
      "id": "d2c47b7321f9198f48a6b32328dd0239"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Tempelhof",
          "id": "900000068201"
        },
        "direction": null,
        "line": {
          "name": "S42",
          "id": null
        },
        "timestamp": "1458835501000",
        "text": "RT @NickPower3939: @ber_ohne_ticket #Fahrkartenkontrolle #s42 #s46 Zwischen Tempelhof und Hermannstraße. Schwarze Windbreaker."
      },
      "geometry": {
        "coordinates": [
          13.385754,
          52.470694
        ],
        "type": "Point"
      },
      "id": "d40d72b7f1b05f21b545756cfb3ae7e2"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Tempelhof",
          "id": "900000068201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1459271795000",
        "text": "RT @NickPower3939: @ber_ohne_ticket Kontrolle zwischen Tempelhof und Hermannstraßr"
      },
      "geometry": {
        "coordinates": [
          13.385754,
          52.470694
        ],
        "type": "Point"
      },
      "id": "d4650a7085455ab6a0f4a0b704753163"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Görlitzer Bahnhof",
          "id": "900000014101"
        },
        "direction": null,
        "line": {
          "name": "U1",
          "id": null
        },
        "timestamp": "1584010216000",
        "text": "RT @Sophlut: #Fahrscheinkontrolle Drei recht junge Kontrolleur*innen (zwei Frauen, ein Mann) Görlitzer Bahnhof. Also wohl u1/u3. Recht schi…"
      },
      "geometry": {
        "coordinates": [
          13.428468,
          52.499035
        ],
        "type": "Point"
      },
      "id": "d5388a561409f74ad3dbc6611638023f"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Neukölln",
          "id": "900000078201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1462033224000",
        "text": "RT @NickPower3939: @ber_ohne_ticket #Kontrolle sbahn #neukölln"
      },
      "geometry": {
        "coordinates": [
          13.441986,
          52.469385
        ],
        "type": "Point"
      },
      "id": "d5a35c9ba95a08e0696274eb56679920"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Baumschulenweg",
          "id": "900000191001"
        },
        "direction": {
          "name": "Westend",
          "id": "900000026207"
        },
        "line": {
          "name": "S46",
          "id": null
        },
        "timestamp": "1574748578000",
        "text": "RT @lysalia_: @noticket_bln Am Baumschulenweg fand gerade ne Ticketkontrolle in der S46 Richtung Westend statt.\n\nSteigen wahrscheinlich in…"
      },
      "geometry": {
        "coordinates": [
          13.489505,
          52.467581
        ],
        "type": "Point"
      },
      "id": "d63067e67860e527fd818764fbc9ac4e"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Eisenacher Str",
          "id": "900000054103"
        },
        "direction": {
          "name": "Rathaus Spandau",
          "id": "900000029302"
        },
        "line": null,
        "timestamp": "1582810240000",
        "text": "RT @DDiekeinerkennt: #noticket_bln U 7 Richtung Rathaus Spandau Eisenacher Straße : graue Jacke , groß , dünn"
      },
      "geometry": {
        "coordinates": [
          13.350276,
          52.489529
        ],
        "type": "Point"
      },
      "id": "d67dd8475abd2e669ab256a568308e45"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Nollendorfplatz",
          "id": "900000056102"
        },
        "direction": null,
        "line": {
          "name": "U1",
          "id": null
        },
        "timestamp": "1497340052000",
        "text": "RT @NickPower3939: @ber_ohne_ticket #Kontrolle am #Nollendorfplatz in #u2 und #u1"
      },
      "geometry": {
        "coordinates": [
          13.353825,
          52.499644
        ],
        "type": "Point"
      },
      "id": "d6d55e12461bc7327a9f9ba96564b34c"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Rosenthaler Platz",
          "id": "900000100023"
        },
        "direction": null,
        "line": {
          "name": "U8",
          "id": null
        },
        "timestamp": "1494573937000",
        "text": "RT @dumbkaido: @ber_ohne_ticket #kontrolle U8, gerade Rosenthaler Platz."
      },
      "geometry": {
        "coordinates": [
          13.401393,
          52.529781
        ],
        "type": "Point"
      },
      "id": "d74787b41b2330f389064ff4ab5bddad"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Weinmeisterstr",
          "id": "900000100051"
        },
        "direction": {
          "name": "Hermannstr",
          "id": "900000079221"
        },
        "line": {
          "name": "U8",
          "id": null
        },
        "timestamp": "1579717177000",
        "text": "RT @ayranallnight: U8 weinmeisterstr. richtung hermannstr #noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.405305,
          52.525376
        ],
        "type": "Point"
      },
      "id": "d7d337ad07d0a048a7300fdd48f349e2"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Alexanderplatz",
          "id": "900000100003"
        },
        "direction": {
          "name": "Westkreuz",
          "id": "900000024102"
        },
        "line": {
          "name": "S5",
          "id": null
        },
        "timestamp": "1581425283000",
        "text": "RT @antifaschwuchtl: Ticketkontrolle in S5 Richtung Westkreuz am Bahnhof Jannowitzbrücke eingestiegen. Ausgestiegen Alexanderplatz. Zwei Mä…"
      },
      "geometry": {
        "coordinates": [
          13.411267,
          52.521512
        ],
        "type": "Point"
      },
      "id": "d9b519caf48df4a45f28c3506a6183e9"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Mierendorffplatz",
          "id": "900000019204"
        },
        "direction": null,
        "line": {
          "name": "U9",
          "id": null
        },
        "timestamp": "1493992227000",
        "text": "RT @weakaside_: @ber_ohne_ticket ticketkontrolle U9 höhe mierendorffplatz, blonde frau und mann rote jacke"
      },
      "geometry": {
        "coordinates": [
          13.305715,
          52.525978
        ],
        "type": "Point"
      },
      "id": "dca883ce5d106dd0f7d4601e71fcea1a"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Tempelhof",
          "id": "900000068201"
        },
        "direction": null,
        "line": null,
        "timestamp": "1581495429000",
        "text": "RT @guatavita73: #noticket_bln Ring s 41 jetzt Tempelhof"
      },
      "geometry": {
        "coordinates": [
          13.385754,
          52.470694
        ],
        "type": "Point"
      },
      "id": "dddbc40d11fa94ef5306295130500f6f"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Westend",
          "id": "900000026207"
        },
        "direction": null,
        "line": {
          "name": "S42",
          "id": null
        },
        "timestamp": "1573815866000",
        "text": "RT @polyamIzzy: Kontrolle in der S42 Ring, gerade an der S Westend - Mensch mit breiten Schultern und Brille, teuer aussehender Jacke in du…"
      },
      "geometry": {
        "coordinates": [
          13.284237,
          52.51861
        ],
        "type": "Point"
      },
      "id": "e3ada06115759aa1f3bfb22ffced2d52"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Hermannplatz",
          "id": "900000078101"
        },
        "direction": null,
        "line": {
          "name": "U7",
          "id": null
        },
        "timestamp": "1574924655000",
        "text": "RT @sasoufilou: Hermannplatz U7 gerade eine Horde BVG Kontrolleur*innen. @noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.42472,
          52.486957
        ],
        "type": "Point"
      },
      "id": "e5b0f18a051bb7239a7e87b3faded557"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Schöneberg",
          "id": "900000054104"
        },
        "direction": null,
        "line": {
          "name": "S41",
          "id": null
        },
        "timestamp": "1580459993000",
        "text": "RT @LaLunaNeko: 4 Kontrolleure auf der s41/42 an der s Schöneberg 3 mit schwarzen Basecap 1 mit weinroten basecap @noticket_bln alle sind u…"
      },
      "geometry": {
        "coordinates": [
          13.352848,
          52.479811
        ],
        "type": "Point"
      },
      "id": "e6d37811117c3aa3879113fb6c3c2863"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Lichterfelde Süd",
          "id": "900000064201"
        },
        "direction": null,
        "line": {
          "name": "S2",
          "id": null
        },
        "timestamp": "1575441508000",
        "text": "RT @lysalia_: @noticket_bln 07:37 S26 teltow\nSind osdorfer eingestiegen gerade bei der lichterfelde Süd P1 trägt ne gelbe jacke\nFahren best…"
      },
      "geometry": {
        "coordinates": [
          13.308662,
          52.409956
        ],
        "type": "Point"
      },
      "id": "e75074d2c9a49618ff905968fb8b0014"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Warschauer Str",
          "id": "900000120004"
        },
        "direction": {
          "name": "Flughafen Berlin-Schönefeld",
          "id": "900000260005"
        },
        "line": {
          "name": "S9",
          "id": null
        },
        "timestamp": "1571930458000",
        "text": "RT @Frau_Koenig: 17.18: S9 Richtung Schönefeld - Höhe Warschauer Str.\nMind. 1 Kontrolleur, schwarze Jacke, dunkelblaue Jeans, mittelgroß.\n#…"
      },
      "geometry": {
        "coordinates": [
          13.448721,
          52.505889
        ],
        "type": "Point"
      },
      "id": "e86ea6754eb6d11530ef6a8585559019"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Frankfurter Allee",
          "id": "900000120001"
        },
        "direction": null,
        "line": null,
        "timestamp": "1583135986000",
        "text": "RT @anarchalist: .@noticket_bln BVG und Bullen auf dem Gleis im U Frankfurter Allee https://t.co/jztEUbzyTO"
      },
      "geometry": {
        "coordinates": [
          13.4753,
          52.513613
        ],
        "type": "Point"
      },
      "id": "e96e939c2b1d5cb3a7933280591fcfe5"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Oranienburger Str",
          "id": "900000100007"
        },
        "direction": null,
        "line": {
          "name": "S1",
          "id": null
        },
        "timestamp": "1507646780000",
        "text": "RT @NickPower3939: @ber_ohne_ticket Fahrscheinkontrolle Brandenburger Tor, Oranienburger straße #s1 #s2 ... Mit polizei?!"
      },
      "geometry": {
        "coordinates": [
          13.393068,
          52.525161
        ],
        "type": "Point"
      },
      "id": "ea2769643777308c7fae1ce755c4f808"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Rosenthaler Platz",
          "id": "900000100023"
        },
        "direction": null,
        "line": {
          "name": "U8",
          "id": null
        },
        "timestamp": "1511198397000",
        "text": "RT @dumbkaido: @ber_ohne_ticket #kontrolle U8 Rosenthaler Platz"
      },
      "geometry": {
        "coordinates": [
          13.401393,
          52.529781
        ],
        "type": "Point"
      },
      "id": "ec709e6facf970e8e5a9b11b14df1b76"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Grünau",
          "id": "900000186001"
        },
        "direction": null,
        "line": {
          "name": "S46",
          "id": null
        },
        "timestamp": "1579021408000",
        "text": "RT @lysalia_: @noticket_bln #noticket_bln\n\n18:00\nS46 Westend\nab grünau\n\n2 Personen\nBeide schwarz gekleidet\nEiner trägt ne Brille\n\nSteigen w…"
      },
      "geometry": {
        "coordinates": [
          13.574018,
          52.412714
        ],
        "type": "Point"
      },
      "id": "eda32a10b4279e3564cfddd5720e5e07"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Wedding",
          "id": "900000009104"
        },
        "direction": null,
        "line": {
          "name": "U6",
          "id": null
        },
        "timestamp": "1477987743000",
        "text": "RT @dumbkaido: @ber_ohne_ticket U6, gerade bei Wedding ausgestiegen. 1 Frau*, 2 Männer*. #kontrolle"
      },
      "geometry": {
        "coordinates": [
          13.36606,
          52.542732
        ],
        "type": "Point"
      },
      "id": "edccb1fb990737aae23f2b58ee1bc111"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Frankfurter Allee",
          "id": "900000120001"
        },
        "direction": null,
        "line": null,
        "timestamp": "1583142319000",
        "text": "RT @anarchalist: Bullen und BVG sind noch im U Frankfurter Allee #noticket_bln https://t.co/r5PW4MaExr"
      },
      "geometry": {
        "coordinates": [
          13.4753,
          52.513613
        ],
        "type": "Point"
      },
      "id": "edfe67ca68d1a09a0b6c635f9b1aa101"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Alexanderplatz",
          "id": "900000100003"
        },
        "direction": {
          "name": "Berlin Hauptbahnhof",
          "id": "900000003201"
        },
        "line": null,
        "timestamp": "1582288987000",
        "text": "RT @Sophlut: #Fahrscheinkontrolle Alexanderplatz Richtung Potsdam hbf #noticket_bln https://t.co/besjG4qCoN"
      },
      "geometry": {
        "coordinates": [
          13.411267,
          52.521512
        ],
        "type": "Point"
      },
      "id": "f07534ede77e09e16b9ed4ff8cd7996a"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Hermannstr",
          "id": "900000079221"
        },
        "direction": null,
        "line": {
          "name": "S42",
          "id": null
        },
        "timestamp": "1455033063000",
        "text": "RT @NickPower3939: @ber_ohne_ticket #kontrolle #s42 ringbahn zwischen Hermannstraße und Tempelhof"
      },
      "geometry": {
        "coordinates": [
          13.431704,
          52.467181
        ],
        "type": "Point"
      },
      "id": "f14bf6db94be71a607b855cc717a4f44"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Möckernbrücke",
          "id": "900000017104"
        },
        "direction": null,
        "line": {
          "name": "U7",
          "id": null
        },
        "timestamp": "1588939312051",
        "text": "@noticket_bln #noticket_bln\nKontrollen an der U7 Möckernbrücke\n\n3-4 Dudes, alle in blauen Westen"
      },
      "geometry": {
        "coordinates": [
          13.383256,
          52.498944
        ],
        "type": "Point"
      },
      "id": "f582ca19d599352770fae27711683684"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Bellevue",
          "id": "900000003102"
        },
        "direction": null,
        "line": null,
        "timestamp": "1573035832000",
        "text": "RT @sistasonDE: Grade wieder verstärkt Kontrollettis unterwegs, Stadtbahn zwischen Bellevue und Friedrichstraße.\n#noticket_bln"
      },
      "geometry": {
        "coordinates": [
          13.347098,
          52.519951
        ],
        "type": "Point"
      },
      "id": "f5bda25353ab954ffdd9d860efc4f223"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Warschauer Str",
          "id": "900000120004"
        },
        "direction": null,
        "line": {
          "name": "U1",
          "id": null
        },
        "timestamp": "1478954335000",
        "text": "RT @NickPower3939: @ber_ohne_ticket Ticketkontrolle #u1 warschauer strasse. Typ mit langem bart."
      },
      "geometry": {
        "coordinates": [
          13.448721,
          52.505889
        ],
        "type": "Point"
      },
      "id": "f8aeb2c19ccbdafc6eb581456aee409e"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Bundesplatz",
          "id": "900000044202"
        },
        "direction": null,
        "line": null,
        "timestamp": "1574861660000",
        "text": "RT @River04Song: #noticket_bln 14.30 bundesplatz ringbahn. Jeans  schwarze jacke"
      },
      "geometry": {
        "coordinates": [
          13.329149,
          52.477366
        ],
        "type": "Point"
      },
      "id": "f9a2bfb9c972f697966191de16cb8aa0"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Tierpark",
          "id": "900000161002"
        },
        "direction": null,
        "line": null,
        "timestamp": "1583839047000",
        "text": "RT @lordmunta: #noticket_bln 12:16 : Vier Kontrolleure im U Bahn Tierpark. 1 mann 3 Frau"
      },
      "geometry": {
        "coordinates": [
          13.523626,
          52.497236
        ],
        "type": "Point"
      },
      "id": "fc8acf99d6ff8762b3a557e8f2610e96"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Südende",
          "id": "900000063452"
        },
        "direction": {
          "name": "Waidmannslust",
          "id": "900000094101"
        },
        "line": {
          "name": "S2",
          "id": null
        },
        "timestamp": "1576147541000",
        "text": "RT @lysalia_: @noticket_bln 11:44\nS26 nach waidmannslust\nAktuell in Lankwitz,\n2 kontrolleure\nSteigen wahrscheinlich Südende aus und fahren…"
      },
      "geometry": {
        "coordinates": [
          13.354077,
          52.448899
        ],
        "type": "Point"
      },
      "id": "fdb21c73cc4a6c337a95d50471364eca"
    },
    {
      "type": "Feature",
      "properties": {
        "station": {
          "name": "Friedrichstr",
          "id": "900000100001"
        },
        "direction": null,
        "line": null,
        "timestamp": "1481727857000",
        "text": "RT @dumbkaido: @ber_ohne_ticket #Kontrolle S Friedrichstraße"
      },
      "geometry": {
        "coordinates": [
          13.387153,
          52.52027
        ],
        "type": "Point"
      },
      "id": "ffd26525d8c0dd6be20b31818f085395"
    }
  ]
};

export default incidents;