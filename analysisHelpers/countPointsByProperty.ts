import { FeatureCollection, featureCollection, Feature } from '@turf/helpers';

type CountedIncidentsDict<F, P> = {
  [key: string]: Feature<F, P & { count: number }>;
};

export const countPointsByProperty = <F, P>(
  incidents: FeatureCollection<F, P>,
  property: string[],
): FeatureCollection<F, P & { count: number }> => {
  // Build dict of counted incidents
  const countedIncidentsDict: CountedIncidentsDict<
    F,
    P
  > = incidents.features.reduce((acc, incident) => {
    // Get the value at the specified property
    const valueAtProperty = property.reduce((acc, prop: string, index) => {
      // If value at property is undefined
      if (!acc[prop]) return {};
      // If acc has property and another property to search for, return value at property
      if (acc[prop] && index < property.length) return acc[prop];
    }, incident.properties);

    // If station already exists in acc
    if (acc[valueAtProperty])
      return {
        ...acc,
        [valueAtProperty]: {
          ...incident,
          properties: {
            ...incident.properties,
            count: acc[valueAtProperty].properties.count + 1,
          },
        },
      };

    // If doesn't exist in acc
    return {
      ...acc,
      [valueAtProperty]: {
        ...incident,
        properties: {
          ...incident.properties,
          count: 1,
        },
      },
    };
  }, {});

  const countedIncidentsList = Object.values(countedIncidentsDict);

  const sortedCountedIncidents = countedIncidentsList.sort(
    (firstIncident, secondIncident) => {
      const { count: firstCount } = firstIncident.properties;
      const { count: secondCount } = secondIncident.properties;
      if (firstCount > secondCount) return -1;
      if (firstCount < secondCount) return 1;
      return 0;
    },
  );

  return featureCollection<F, P & { count: number }>(sortedCountedIncidents);
};
