# Data Cities - Mapping Control Workshop Notebooks

This document is to help with setting up the local environment for running this notebook. It isn't part of the workshop contents! Don't worry about this if you're not running the notebooks locally.

## Setup

*Requirements:* You will need Python, Pip, and Node installed on your machine.

1. Install jupyter.
  - `pip install jupyterlab`
2. Install tslab (typescript kernel for jupyter lab).
  - [See instructions here.](https://github.com/yunabe/tslab#installing-tslab-1)
3. Install Node dependencies
  - `npm install`
4. Run jupyter lab
  - `jupyter lab`
  
